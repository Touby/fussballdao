package FussballXXL.dataLayer.dataAccessObjects;

import FussballXXL.businessObjects.IMannschaft;
import FussballXXL.exceptions.DaoException;

public interface IMannschaftDao {
	public void create(IMannschaft mannschaft) throws DaoException;
	public IMannschaft read(int id) throws DaoException;
	public void update(IMannschaft mannschaft) throws DaoException;
	public void delete(IMannschaft mannschaft) throws DaoException;
}
