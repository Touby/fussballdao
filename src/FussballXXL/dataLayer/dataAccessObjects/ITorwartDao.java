package FussballXXL.dataLayer.dataAccessObjects;

import FussballXXL.businessObjects.ITorwart;
import FussballXXL.exceptions.DaoException;

public interface ITorwartDao {
	public void create(ITorwart torwart) throws DaoException;
	public ITorwart read(int id) throws DaoException;
	public void update(ITorwart torwart) throws DaoException;
	public void delete (ITorwart torwart) throws DaoException;
}
