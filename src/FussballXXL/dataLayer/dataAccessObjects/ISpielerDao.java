package FussballXXL.dataLayer.dataAccessObjects;

import java.util.ArrayList;

import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import FussballXXL.businessObjects.ISpieler;
import FussballXXL.exceptions.DaoException;

public interface ISpielerDao {
	public void create(ISpieler spieler, int mannschaftId) throws DaoException;
	public ISpieler read(int id) throws DaoException;
	public ArrayList<ISpieler> readTeam(int mannschaftId) throws DaoException;
	public void update(ISpieler spieler) throws DaoException;
	public void update (ISpieler spieler, int mannschaftId) throws DaoException;
	public void delete (ISpieler spieler) throws DaoException;
}