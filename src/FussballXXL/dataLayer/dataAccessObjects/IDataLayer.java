package FussballXXL.dataLayer.dataAccessObjects;

public interface IDataLayer {
	public IMannschaftDao getMannschaftDao();
	public ITrainerDao getTrainerDao();
	public ITorwartDao getTorwartDao();
	public ISpielerDao getSpielerDao();
}
