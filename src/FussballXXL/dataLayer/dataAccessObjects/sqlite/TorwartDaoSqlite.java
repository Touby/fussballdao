package FussballXXL.dataLayer.dataAccessObjects.sqlite;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import FussballXXL.businessObjects.ITorwart;
import FussballXXL.dataLayer.businessObjects.Torwart;
import FussballXXL.dataLayer.dataAccessObjects.ITorwartDao;

public class TorwartDaoSqlite implements ITorwartDao{
	
	@Override
	public void create(ITorwart torwart){
		String name;
		int alter, staerke, torschuss, motivation, tore, reaktion;
		try (Connection connect = ConnectionManager.getNewConnection()) {
			String query = "INSERT INTO torwart('name','alter','staerke','torschuss','motivation','tore','reaktion') VALUES(?,?,?,?,?,?,?)";
			
			alter = torwart.getAlter();
			name = torwart.getName();
			staerke = torwart.getStaerke();
			torschuss = torwart.getTorschuss();
			motivation = torwart.getMotivation();
			tore = torwart.getTore();
			reaktion = torwart.getReaktion();
			
			PreparedStatement fill = connect.prepareStatement(query);
			fill.setString(1, name);
			fill.setInt(2, alter);
			fill.setInt(3, staerke);
			fill.setInt(4, torschuss);
			fill.setInt(5, motivation);
			fill.setInt(6, tore);
			fill.setInt(7, reaktion);
			fill.executeUpdate();
			
			ConnectionManager.close(fill, connect);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public ITorwart read(int id) {
		Torwart torwart = null;
		try (Connection connect = ConnectionManager.getNewConnection()) {
			String name;
			int alter, staerke, motivation, reaktion;
			
			String query = "SELECT * FROM torwart WHERE id = ?";
			PreparedStatement fill = connect.prepareStatement(query);
			fill.setInt(1, id);
			ResultSet rs = fill.executeQuery();
			
			name = rs.getString("name");
			alter = rs.getInt("alter");
			staerke = rs.getInt("staerke");
			motivation = rs.getInt("motivation");
			reaktion = rs.getInt("reaktion");
			
			torwart = new Torwart(name,alter,staerke,motivation,reaktion);
			
			ConnectionManager.close(fill, connect);
			
			return torwart;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return torwart;
	}

	@Override
	public void update(ITorwart torwart) {
		
		Scanner scanner = new Scanner(System.in);
		int updateChoice;
		
		try (Connection connect = ConnectionManager.getNewConnection()) {
			String query = "";
			String name = "";
			int var;
			
			System.out.println("Update Torwart:\n1: Update Name\n2: Update Alter\n3: Update Staerke\n4: Update Torschuss\n5: Update Motivation\n6: Update Tore\n7: Update Reaktion");
			updateChoice = scanner.nextInt();
			switch(updateChoice) 
			{
				case 1:
					System.out.println("Update Name: ");
					query = "UPDATE torwart SET 'name' = ? WHERE 'name' = ?";
					name = scanner.nextLine();
					break;
				case 2:
					System.out.println("Update Alter: ");
					query = "UPDATE torwart SET 'alter' = ? WHERE 'name' = ?";
					break;
				case 3:
					System.out.println("Update Staerke: ");
					query = "UPDATE torwart SET 'staerke' = ? WHERE 'name' = ?";
					break;
				case 4:
					System.out.println("Update Torschuss: ");
					query = "UPDATE torwart SET 'torschuss' = ? WHERE 'name' = ?";
					break;
				case 5:
					System.out.println("Update Motivation: ");
					query = "UPDATE torwart SET 'motivation' = ? WHERE 'name' = ?";
					break;
				case 6:
					System.out.println("Update Tore: ");
					query = "UPDATE torwart SET 'tore' = ? WHERE 'name' = ?";
					break;
				case 7:
					System.out.println("Update Reaktion: ");
					query = "UPDATE torwart SET 'reaktion' = ? WHERE 'name' = ?";
					break;
				default:
					System.out.println("Nichts");
					break;
			}
			
			PreparedStatement fill = connect.prepareStatement(query);
			
			if(updateChoice > 1 && updateChoice < 8) {
				var = scanner.nextInt();
				fill.setInt(1, var);
			} else if (updateChoice == 1) {
				fill.setString(1, name);
			}
			
			fill.setString(2, torwart.getName());
			fill.executeUpdate();
			scanner.close();
			
			ConnectionManager.close(fill, connect);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void delete(ITorwart torwart) {
	
		try (Connection connect = ConnectionManager.getNewConnection()) {
			
			String query = "DELETE FROM torwart WHERE name = ?";
			
			PreparedStatement fill = connect.prepareStatement(query);
			fill.setString(1, torwart.getName());
			
			fill.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
