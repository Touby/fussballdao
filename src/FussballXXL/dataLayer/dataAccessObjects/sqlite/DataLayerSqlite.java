package FussballXXL.dataLayer.dataAccessObjects.sqlite;
import FussballXXL.dataLayer.dataAccessObjects.IDataLayer;
import FussballXXL.dataLayer.dataAccessObjects.IMannschaftDao;
import FussballXXL.dataLayer.dataAccessObjects.ISpielerDao;
import FussballXXL.dataLayer.dataAccessObjects.ITorwartDao;
import FussballXXL.dataLayer.dataAccessObjects.ITrainerDao;

public class DataLayerSqlite implements IDataLayer {
	
	IMannschaftDao mannschaftDao = new MannschaftDaoSqlite();
	ITrainerDao trainerDao = new TrainerDaoSqlite();
	ITorwartDao torwartDao = new TorwartDaoSqlite();
	ISpielerDao spielerDao = new SpielerDaoSqlite();

	@Override
	public IMannschaftDao getMannschaftDao() {

		return mannschaftDao;
	}

	@Override
	public ITrainerDao getTrainerDao() {
		
		return trainerDao;
	}

	@Override
	public ITorwartDao getTorwartDao() {
		
		return torwartDao;
	}

	@Override
	public ISpielerDao getSpielerDao() {
		
		return spielerDao;
	}
}