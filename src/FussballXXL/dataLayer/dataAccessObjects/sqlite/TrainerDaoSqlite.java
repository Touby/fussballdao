package FussballXXL.dataLayer.dataAccessObjects.sqlite;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import FussballXXL.businessObjects.ITrainer;
import FussballXXL.dataLayer.businessObjects.Trainer;
import FussballXXL.dataLayer.dataAccessObjects.ITrainerDao;

public class TrainerDaoSqlite implements ITrainerDao{
	
	@Override
	public void create(ITrainer trainer){
		String name;
		int alter, erfahrung;
		try (Connection connect = ConnectionManager.getNewConnection()) {
			String query = "INSERT INTO trainer('name','alter','erfahrung') VALUES(?,?,?)";
			
			alter = trainer.getAlter();
			name = trainer.getName();
			erfahrung = trainer.getErfahrung();
			
			PreparedStatement fill = connect.prepareStatement(query);
			fill.setString(1, name);
			fill.setInt(2, alter);
			fill.setInt(3, erfahrung);
			fill.executeUpdate();
			
			ConnectionManager.close(fill, connect);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public ITrainer read(int id) {
		Trainer trainer = null;
		try (Connection connect = ConnectionManager.getNewConnection()) {
			String name;
			int alter, erfahrung;
			
			String query = "SELECT * FROM trainer WHERE id = ?";
			PreparedStatement fill = connect.prepareStatement(query);
			fill.setInt(1, id);
			ResultSet rs = fill.executeQuery();
			
			name = rs.getString("name");
			alter = rs.getInt("alter");
			erfahrung = rs.getInt("erfahrung");
			
			trainer = new Trainer(name,alter,erfahrung);
			
			ConnectionManager.close(fill, connect);
			
			return trainer;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return trainer;
	}

	@Override
	public void update(ITrainer trainer) {
		
		Scanner scanner = new Scanner(System.in);
		int updateChoice;
		
		try (Connection connect = ConnectionManager.getNewConnection()) {
			String query = "";
			String name = "";
			int var;
			
			System.out.println("Update Trainer:\n1: Update Name\n2: Update Alter\n3: Update Erfahrung");
			updateChoice = scanner.nextInt();
			switch(updateChoice) 
			{
				case 1:
					System.out.println("Update Name: ");
					query = "UPDATE trainer SET 'name' = ? WHERE 'name' = ?";
					name = scanner.nextLine();
					break;
				case 2:
					System.out.println("Update Alter: ");
					query = "UPDATE trainer SET 'alter' = ? WHERE 'name' = ?";
					break;
				case 3:
					System.out.println("Update Erfahrung: ");
					query = "UPDATE trainer SET 'erfahrung' = ? WHERE 'name' = ?";
					break;
				default:
					System.out.println("Nichts");
					break;
			}
			
			PreparedStatement fill = connect.prepareStatement(query);
			
			if(updateChoice > 1 && updateChoice < 4) {
				var = scanner.nextInt();
				fill.setInt(1, var);
			} else if (updateChoice == 1) {
				fill.setString(1, name);
			}
			
			fill.setString(2, trainer.getName());
			fill.executeUpdate();
			scanner.close();
			
			ConnectionManager.close(fill, connect);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void delete(ITrainer trainer) {
	
		try (Connection connect = ConnectionManager.getNewConnection()) {
			
			String query = "DELETE FROM trainer WHERE 'name' = ?";
			
			PreparedStatement fill = connect.prepareStatement(query);
			fill.setString(1, trainer.getName());
			
			fill.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
