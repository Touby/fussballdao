package FussballXXL.dataLayer.dataAccessObjects.sqlite;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import FussballXXL.businessObjects.IMannschaft;
import FussballXXL.businessObjects.ISpieler;
import FussballXXL.businessObjects.ITorwart;
import FussballXXL.businessObjects.ITrainer;
import FussballXXL.dataLayer.businessObjects.Mannschaft;
import FussballXXL.dataLayer.businessObjects.Spieler;
import FussballXXL.dataLayer.dataAccessObjects.IMannschaftDao;
import FussballXXL.dataLayer.dataAccessObjects.sqlite.SpielerDaoSqlite;
import FussballXXL.dataLayer.dataAccessObjects.sqlite.TrainerDaoSqlite;
import FussballXXL.exceptions.DaoException;
import FussballXXL.dataLayer.dataAccessObjects.sqlite.TorwartDaoSqlite;

public class MannschaftDaoSqlite implements IMannschaftDao {
	@Override
	public void create(IMannschaft mannschaft){
		String name;
		ITrainer trainer = null;
		ITorwart torwart = null;
		
		try (Connection connect = ConnectionManager.getNewConnection()) {
			String query = "INSERT INTO mannschaft('name','trainer_id','torwart_id') VALUES(?,?,?)";
			
			name = mannschaft.getName();
			trainer = mannschaft.getTrainer();
			torwart = mannschaft.getTorwart();
			
			PreparedStatement fill = connect.prepareStatement(query);
			fill.setString(1, name);
			fill.setInt(2, trainer.getId());
			fill.setInt(3, torwart.getId());
			fill.executeUpdate();
			
			ConnectionManager.close(fill, connect);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public IMannschaft read(int id) {
		IMannschaft mannschaft = null;
		ITrainer trainer = null;
		ITorwart torwart = null;
		SpielerDaoSqlite team = new SpielerDaoSqlite();
		TrainerDaoSqlite getTrainer = new TrainerDaoSqlite();
		TorwartDaoSqlite getTorwart = new TorwartDaoSqlite();
		ArrayList<ISpieler> spielerListe = team.readTeam(id);
			
		try (Connection connect = ConnectionManager.getNewConnection()) {
			String name;
			int trainer_id, torwart_id;
			
			String query = "SELECT * FROM mannschaft WHERE id = ?";
			PreparedStatement fill = connect.prepareStatement(query);
			fill.setInt(1, id);
			ResultSet rs = fill.executeQuery();
			
			name = rs.getString("name");
			trainer_id = rs.getInt("trainer_id");
			torwart_id = rs.getInt("torwart_id");
			
			// get Trainer
			trainer = getTrainer.read(trainer_id);
			
			// get Torwart
			torwart = getTorwart.read(torwart_id);
			
			
			mannschaft = new Mannschaft(name,trainer,torwart,spielerListe);
			
			ConnectionManager.close(fill, connect);
			
			return mannschaft;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return mannschaft;
	}

	@Override
	public void update(IMannschaft mannschaft) {
		
		Scanner scanner = new Scanner(System.in);
		int updateChoice;
		
		try (Connection connect = ConnectionManager.getNewConnection()) {
			String query = "";
			String name = "";
			int var;
			
			System.out.println("Update Mannschaft:\n1: Update Name\n2: Update Trainer\n3: Update Torwart");
			updateChoice = scanner.nextInt();
			switch(updateChoice) 
			{
				case 1:
					System.out.println("Update Name: ");
					query = "UPDATE mannschaft SET 'name' = ? WHERE 'name' = ?";
					name = scanner.nextLine();
					break;
				case 2:
					System.out.println("Update Trainer: ");
					query = "UPDATE mannschaft SET 'trainer_id' = ? WHERE 'name' = ?";
					break;
				case 3:
					System.out.println("Update Torwart: ");
					query = "UPDATE mannschaft SET 'torwart_id' = ? WHERE 'name' = ?";
					break;
				default:
					System.out.println("Nichts");
					break;
			}
			
			PreparedStatement fill = connect.prepareStatement(query);
			
			if(updateChoice > 1 && updateChoice < 4) {
				var = scanner.nextInt();
				fill.setInt(1, var);
			} else if (updateChoice == 1) {
				fill.setString(1, name);
			}
			
			fill.setString(2, mannschaft.getName());
			fill.executeUpdate();
			scanner.close();
			
			ConnectionManager.close(fill, connect);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void delete(IMannschaft mannschaft) {
	
		try (Connection connect = ConnectionManager.getNewConnection()) {
			
			String query = "DELETE FROM mannschaft WHERE 'name' = ?";
			
			PreparedStatement fill = connect.prepareStatement(query);
			fill.setString(1, mannschaft.getName());
			
			fill.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws DaoException 
	{
		MannschaftDaoSqlite test = new MannschaftDaoSqlite();
		IMannschaft mannschaft = test.read(2);
		System.out.println(mannschaft);
		//test.update(test.read(2));

	}
}
