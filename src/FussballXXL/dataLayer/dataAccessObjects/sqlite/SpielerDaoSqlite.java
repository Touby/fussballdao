package FussballXXL.dataLayer.dataAccessObjects.sqlite;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import FussballXXL.businessObjects.ISpieler;
import FussballXXL.dataLayer.businessObjects.Spieler;
import FussballXXL.dataLayer.dataAccessObjects.ISpielerDao;
import FussballXXL.exceptions.DaoException;

public class SpielerDaoSqlite implements ISpielerDao{
	
	@Override
	public void create(ISpieler spieler, int mannschaftId){
		String name;
		int alter, staerke, torschuss, motivation, tore;
		try (Connection connect = ConnectionManager.getNewConnection()) {
			String query = "INSERT INTO spieler('name','alter','staerke','torschuss','motivation','tore') VALUES(?,?,?,?,?,?)";
			
			alter = spieler.getAlter();
			name = spieler.getName();
			staerke = spieler.getStaerke();
			torschuss = spieler.getTorschuss();
			motivation = spieler.getMotivation();
			tore = spieler.getTore();
			
			PreparedStatement fill = connect.prepareStatement(query);
			fill.setString(1, name);
			fill.setInt(2, alter);
			fill.setInt(3, staerke);
			fill.setInt(4, torschuss);
			fill.setInt(5, motivation);
			fill.setInt(6, tore);
			fill.executeUpdate();
			
			ConnectionManager.close(fill, connect);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public ISpieler read(int id) {
		Spieler spieler = null;
		try (Connection connect = ConnectionManager.getNewConnection()) {
			String name;
			int alter, staerke, torschuss, motivation, tore;
			
			String query = "SELECT * FROM spieler WHERE id = ?";
			PreparedStatement fill = connect.prepareStatement(query);
			fill.setInt(1, id);
			ResultSet rs = fill.executeQuery();
			
			name = rs.getString("name");
			alter = rs.getInt("alter");
			staerke = rs.getInt("staerke");
			torschuss = rs.getInt("torschuss");
			motivation = rs.getInt("motivation");
			tore = rs.getInt("tore");
			
			spieler = new Spieler(name,alter,staerke,torschuss,motivation,tore);
			
			ConnectionManager.close(fill, connect);
			
			return spieler;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return spieler;
	}

	@Override
	public void update(ISpieler spieler) {
		
		Scanner scanner = new Scanner(System.in);
		int updateChoice;
		
		try (Connection connect = ConnectionManager.getNewConnection()) {
			String query = "";
			String name = "";
			int var;
			
			System.out.println("Update Spieler:\n1: Update Name\n2: Update Alter\n3: Update Staerke\n4: Update Torschuss\n5: Update Motivation\n6: Update Tore");
			updateChoice = scanner.nextInt();
			switch(updateChoice) 
			{
				case 1:
					System.out.println("Update Name: ");
					query = "UPDATE spieler SET name = ? WHERE name = ?";
					name = scanner.nextLine();
					break;
				case 2:
					System.out.println("Update Alter: ");
					query = "UPDATE spieler SET alter = ? WHERE name = ?";
					break;
				case 3:
					System.out.println("Update Staerke: ");
					query = "UPDATE spieler SET staerke = ? WHERE name = ?";
					break;
				case 4:
					System.out.println("Update Torschuss: ");
					query = "UPDATE spieler SET torschuss = ? WHERE name = ?";
					break;
				case 5:
					System.out.println("Update Motivation: ");
					query = "UPDATE spieler SET motivation = ? WHERE name = ?";
					break;
				case 6:
					System.out.println("Update Tore: ");
					query = "UPDATE spieler SET tore = ? WHERE name = ?";
					break;
				default:
					System.out.println("Nichts");
					break;
			}
			
			PreparedStatement fill = connect.prepareStatement(query);
			
			if(updateChoice > 1 && updateChoice < 7) {
				var = scanner.nextInt();
				fill.setInt(1, var);
			} else if (updateChoice == 1) {
				fill.setString(1, name);
			}
			
			fill.setString(2, spieler.getName());
			fill.executeUpdate();
			scanner.close();
			
			ConnectionManager.close(fill, connect);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void update(ISpieler spieler, int mannschaftId) {
		
		try (Connection connect = ConnectionManager.getNewConnection()) {
			
			String query = "UPDATE spieler SET mannschaft_id = ? WHERE name = ?";
			PreparedStatement fill = connect.prepareStatement(query);
			
			fill.setInt(1, mannschaftId);
			fill.setString(2, spieler.getName());
			fill.executeUpdate();
			
			ConnectionManager.close(fill, connect);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void delete(ISpieler spieler) {
	
		try (Connection connect = ConnectionManager.getNewConnection()) {
			
			String query = "DELETE FROM spieler WHERE name = ?";
			
			PreparedStatement fill = connect.prepareStatement(query);
			fill.setString(1, spieler.getName());
			
			fill.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public ArrayList<ISpieler> readTeam(int mannschaftId) {
		ArrayList<ISpieler> spielerListe = new ArrayList<ISpieler>();
		
		try (Connection connect = ConnectionManager.getNewConnection()) {
			
			String query = "SELECT * FROM spieler WHERE mannschaft_id = ?";
			PreparedStatement fill = connect.prepareStatement(query);
			fill.setInt(1, mannschaftId);
			
            ResultSet rs = fill.executeQuery();
            
            while(rs.next()) {
            	Spieler spieler1 = new Spieler(
						rs.getString("name"),
						rs.getInt("alter"),
						rs.getInt("staerke"),
						rs.getInt("torschuss"),
						rs.getInt("motivation"),
						rs.getInt("tore")
						);
				spieler1.setId(rs.getInt("id"));
				spielerListe.add(spieler1);
            }
			
            ConnectionManager.close(fill, connect);
            
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return spielerListe;
	}
	
	public static void main(String[] args) throws DaoException 
	{
		SpielerDaoSqlite test = new SpielerDaoSqlite();
		ISpieler spieler = test.read(2);
		System.out.println(spieler);
		//test.update(test.read(2));
		test.readTeam(1);
		System.out.println(test.readTeam(1));
	}
}
