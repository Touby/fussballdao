package FussballXXL.dataLayer.dataAccessObjects.sqlite;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectionManager {
	static String path = "jdbc:sqlite:Fussball.db";
	static Connection sql = null;		
	
	public static Connection getNewConnection() {
		
		try {
			sql = DriverManager.getConnection(path);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return sql;
	}
	
	public static void close(Statement statement, Connection connection) {
		try {
			if(!sql.isClosed()) {
				if(!statement.isClosed()) {
					statement.close();
				}
				sql.close();
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
