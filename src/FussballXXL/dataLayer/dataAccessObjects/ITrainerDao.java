package FussballXXL.dataLayer.dataAccessObjects;

import FussballXXL.businessObjects.ITrainer;
import FussballXXL.exceptions.DaoException;

public interface ITrainerDao {
	public void create(ITrainer trainer) throws DaoException;
	public ITrainer read(int id) throws DaoException;
	public void update(ITrainer trainer) throws DaoException;
	public void delete(ITrainer trainer) throws DaoException;
}