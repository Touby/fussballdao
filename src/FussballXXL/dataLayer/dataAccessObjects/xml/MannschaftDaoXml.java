package FussballXXL.dataLayer.dataAccessObjects.xml;

import FussballXXL.businessObjects.IMannschaft;

import java.io.File;
import java.util.Scanner;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import FussballXXL.dataLayer.businessObjects.Mannschaft;
import FussballXXL.dataLayer.dataAccessObjects.IMannschaftDao;
import FussballXXL.exceptions.DaoException;

public class MannschaftDaoXml implements IMannschaftDao{
	
	Document doc = XmlParser.parseXmlFile();

	public MannschaftDaoXml()
	{
		
	}
	
	@Override
	public void create(IMannschaft mannschaft) {
		try 
		{
			//find out the latest id so the id of the new spieler can be set 1 higher
			int lastId = 0; 
			NodeList nMannschaftList = doc.getElementsByTagName("mannschaft");
			 
			for(int temp = 0; temp < nMannschaftList.getLength(); temp++)
			{
				Node node = nMannschaftList.item(temp);
				if(node.getNodeType() == Node.ELEMENT_NODE)
				{
					Element element = (Element) node;
					int id = Integer.parseInt(element.getAttribute("id"));
					lastId = id;
				}
			}
			

			//create new spieler
			Scanner scanner = new Scanner(System.in);
			NodeList nList = doc.getElementsByTagName("mannschaft");
			Node parentNode = nList.item(0).getParentNode();
			
			Element newMannschaft = doc.createElement("mannschaft");
			newMannschaft.setAttribute("id", Integer.toString(lastId + 1));
			System.out.println("Welche TorwartId bekommt der neue Spieler?");
			newMannschaft.setAttribute("torwart", Integer.toString(mannschaft.getTorwart().getId()));
			System.out.println("Welche MannschaftsId bekommt der neue Spieler?");
			newMannschaft.setAttribute("trainer", Integer.toString(mannschaft.getTrainer().getId()));
			scanner.close();
			
			Element newName = doc.createElement("name");
			newName.setTextContent(mannschaft.getName());

			
			newMannschaft.appendChild(newName);
			parentNode.appendChild(newMannschaft);
			
			
			//remove every blank line so they can be added later with constant distance
			XPathFactory xfact = XPathFactory.newInstance();
			XPath xpath = xfact.newXPath();
			NodeList empty = (NodeList)xpath.evaluate("//text()[normalize-space(.) = '']", doc, XPathConstants.NODESET);
			
			for(int i = 0; i < empty.getLength(); i++)
			{
				Node node = empty.item(i);
				node.getParentNode().removeChild(node);
			}
		
			
			//transform xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(XmlParser.filename));
			transformer.transform(source, result);
			System.out.println("Node Created!");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public IMannschaft read(int id) throws DaoException {
		
		Element mannschaft = null;
		int torwartId = 0;
		int trainerId = 0;
		TorwartDaoXml todx = new TorwartDaoXml();
		SpielerDaoXml spdx = new SpielerDaoXml();
		TrainerDaoXml trdx = new TrainerDaoXml();
		 
		 NodeList mannschaftList = doc.getElementsByTagName("mannschaft");
		 
		 for(int temp = 0; temp < mannschaftList.getLength(); temp++)
		 {
			 Node node = mannschaftList.item(temp);
			 if(node.getNodeType() == Node.ELEMENT_NODE)
			 {
				 Element element = (Element) node;
				 if(Integer.parseInt(element.getAttribute("id")) == id)
				 {
				 	mannschaft = element;
				 	torwartId = Integer.parseInt(mannschaft.getAttribute("torwart"));
				 	trainerId = Integer.parseInt(mannschaft.getAttribute("trainer"));
				 	break;
				 }
			 }
		 }
		
		 Mannschaft mannschaft1 = new Mannschaft(
					mannschaft.getElementsByTagName("name").item(0).getTextContent(),
					trdx.read(trainerId),
					todx.read(torwartId),
					spdx.readTeam(id)
					);
		 mannschaft1.setId(Integer.parseInt(mannschaft.getAttribute("id")));
		 return mannschaft1;
	}

	@Override
	public void update(IMannschaft mannschaft) {
		
		Scanner scanner = new Scanner(System.in);
		Scanner scanner2 = new Scanner(System.in);
		int updateChoice;
		
		try 
		{
			NodeList nList = doc.getElementsByTagName("mannschaft");
		
			for(int temp = 0; temp < nList.getLength(); temp++)
			{
				Node node = nList.item(temp);
				if(node.getNodeType() == Node.ELEMENT_NODE)
				{
					Element element = (Element) node;
					if(element.getElementsByTagName("name").item(0).getTextContent().equals(mannschaft.getName()))
					{
						System.out.println("Update Mannschaft:\n1: Update Name");
						updateChoice = scanner.nextInt();
						switch(updateChoice) 
						{
							case 1:
								System.out.println("Update Name: ");
								element.getElementsByTagName("name").item(0).setTextContent(scanner2.next());
								break;
							default:
								System.out.println("Nichts");
								break;
						}
					}
				}
			}
			
			scanner.close();
			scanner2.close();
		
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(XmlParser.filename));
			transformer.transform(source, result);
			System.out.println("File Update Done!");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void delete(IMannschaft mannschaft) {
		
		try 
		{
			NodeList nList = doc.getElementsByTagName("mannschaft");
			
			for(int temp = 0; temp < nList.getLength(); temp++)
			{
				Node node = nList.item(temp);
				if(node.getNodeType() == Node.ELEMENT_NODE)
				{
					Element element = (Element) node;
					if(element.getElementsByTagName("name").item(0).getTextContent().equals(mannschaft.getName()))
					{
						element.getParentNode().removeChild(element);
					}
				}
			}
		
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(XmlParser.filename));
			transformer.transform(source, result);
			System.out.println("Node Deleted!");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
				
	}

	

	public static void main(String[] args) throws DaoException {

		MannschaftDaoXml test = new MannschaftDaoXml();
		IMannschaft mannschaft = new Mannschaft("Pimmelberger AC", test.read(1).getTrainer(), test.read(1).getTorwart(), test.read(1).getSpielerListe());
		
		//test
		System.out.println(test.read(1).getName());
		System.out.println(test.read(1).getTorwart());
		System.out.println(test.read(1).getSpielerListe());
		System.out.println(test.read(1).getTrainer());
		test.create(mannschaft);
		//test.update(test.read(0));
		//test.delete(test.read(2));
	}
}