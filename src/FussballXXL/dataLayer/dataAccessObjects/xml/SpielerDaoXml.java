package FussballXXL.dataLayer.dataAccessObjects.xml;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import FussballXXL.businessObjects.ISpieler;
import FussballXXL.businessObjects.ITorwart;
import FussballXXL.dataLayer.businessObjects.Spieler;
import FussballXXL.dataLayer.dataAccessObjects.ISpielerDao;
import FussballXXL.exceptions.DaoException;

public class SpielerDaoXml implements ISpielerDao {
	
	Document doc = XmlParser.parseXmlFile();

	@Override
	public void create(ISpieler spieler, int mannschaftId){
		
		try 
		{
			//find out the latest id so the id of the new spieler can be set 1 higher
			int lastId = 0; 
			NodeList nSpielerList = doc.getElementsByTagName("spieler");
			 
			for(int temp = 0; temp < nSpielerList.getLength(); temp++)
			{
				Node node = nSpielerList.item(temp);
				if(node.getNodeType() == Node.ELEMENT_NODE)
				{
					Element element = (Element) node;
					int id = Integer.parseInt(element.getAttribute("id"));
					lastId = id;
				}
			}
			

			//create new spieler
			Scanner scanner = new Scanner(System.in);
			NodeList nList = doc.getElementsByTagName("spieler");
			Node parentNode = nList.item(0).getParentNode();
			
			Element newSpieler = doc.createElement("spieler");
			newSpieler.setAttribute("id", Integer.toString(lastId + 1));
			//System.out.println("Welche MannschaftsId bekommt der neue Spieler?");
			//newSpieler.setAttribute("mannschaft", Integer.toString(scanner.nextInt()));
			newSpieler.setAttribute("mannschaft", Integer.toString(mannschaftId));
			scanner.close();
			
			Element newName = doc.createElement("name");
			newName.setTextContent(spieler.getName());
			
			Element newAlter = doc.createElement("alter");
			newAlter.setTextContent(Integer.toString(spieler.getAlter()));
			
			Element newMotivation = doc.createElement("motivation");
			newMotivation.setTextContent(Integer.toString(spieler.getMotivation()));
			
			Element newStaerke = doc.createElement("staerke");
			newStaerke.setTextContent(Integer.toString(spieler.getStaerke()));
			
			Element newTore = doc.createElement("tore");
			newTore.setTextContent(Integer.toString(spieler.getTore()));
			
			Element newTorschuss = doc.createElement("torschuss");
			newTorschuss.setTextContent(Integer.toString(spieler.getTorschuss()));

			
			newSpieler.appendChild(newName);
			newSpieler.appendChild(newAlter);
			newSpieler.appendChild(newMotivation);
			newSpieler.appendChild(newStaerke);
			newSpieler.appendChild(newTore);
			newSpieler.appendChild(newTorschuss);
			parentNode.appendChild(newSpieler);
			
			
			//remove every blank line so they can be added later with constant distance
			XPathFactory xfact = XPathFactory.newInstance();
			XPath xpath = xfact.newXPath();
			NodeList empty = (NodeList)xpath.evaluate("//text()[normalize-space(.) = '']", doc, XPathConstants.NODESET);
			
			for(int i = 0; i < empty.getLength(); i++)
			{
				Node node = empty.item(i);
				node.getParentNode().removeChild(node);
			}
		
			
			//transform xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(XmlParser.filename));
			transformer.transform(source, result);
			System.out.println("Node Created!");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public ISpieler read(int id) {

		Element spieler = null;
		 
		NodeList nList = doc.getElementsByTagName("spieler");
		 
		for(int temp = 0; temp < nList.getLength(); temp++)
		{
			Node node = nList.item(temp);
			if(node.getNodeType() == Node.ELEMENT_NODE)
			{
				Element element = (Element) node;
				if(Integer.parseInt(element.getAttribute("id")) == id)
				{
					spieler = element;
					break;
				}
			}
		}
		Spieler spieler1 = new Spieler(
				spieler.getElementsByTagName("name").item(0).getTextContent(),
				Integer.parseInt(spieler.getElementsByTagName("alter").item(0).getTextContent()),
				Integer.parseInt(spieler.getElementsByTagName("staerke").item(0).getTextContent()),
				Integer.parseInt(spieler.getElementsByTagName("torschuss").item(0).getTextContent()),
				Integer.parseInt(spieler.getElementsByTagName("motivation").item(0).getTextContent()),
				Integer.parseInt(spieler.getElementsByTagName("tore").item(0).getTextContent())
				);
		spieler1.setId(Integer.parseInt(spieler.getAttribute("id")));
		return spieler1;
	}

	@Override
	public ArrayList<ISpieler> readTeam(int mannschaftId) {
		
		ArrayList<ISpieler> spielerListe = new ArrayList<ISpieler>();
		
		Element spieler = null;
		
		NodeList nList = doc.getElementsByTagName("spieler");
		 
		for(int temp = 0; temp < nList.getLength(); temp++)
		{
			Node node = nList.item(temp);
			if(node.getNodeType() == Node.ELEMENT_NODE)
			{
				Element element = (Element) node;
				if(Integer.parseInt(element.getAttribute("mannschaft")) == mannschaftId)
				{
					spieler = element;
					Spieler spieler1 = new Spieler(
							spieler.getElementsByTagName("name").item(0).getTextContent(),
							Integer.parseInt(spieler.getElementsByTagName("alter").item(0).getTextContent()),
							Integer.parseInt(spieler.getElementsByTagName("staerke").item(0).getTextContent()),
							Integer.parseInt(spieler.getElementsByTagName("torschuss").item(0).getTextContent()),
							Integer.parseInt(spieler.getElementsByTagName("motivation").item(0).getTextContent()),
							Integer.parseInt(spieler.getElementsByTagName("tore").item(0).getTextContent())
							);
					spieler1.setId(Integer.parseInt(spieler.getAttribute("id")));
					spielerListe.add(spieler1);
				}
			}
		}
		
		return spielerListe;
	}

	@Override
	public void update(ISpieler spieler) 
	{
		try 
		{
			NodeList nList = doc.getElementsByTagName("spieler");
		
			for(int temp = 0; temp < nList.getLength(); temp++)
			{
				Node node = nList.item(temp);
				if(node.getNodeType() == Node.ELEMENT_NODE)
				{
					Element element = (Element) node;
					if(element.getElementsByTagName("name").item(0).getTextContent().equals(spieler.getName()))
					{
						element.getElementsByTagName("name").item(0).setTextContent(spieler.getName());
						if(spieler.getAlter() != -1) element.getElementsByTagName("alter").item(0).setTextContent(Integer.toString(spieler.getAlter()));
						if(spieler.getStaerke() != -1) element.getElementsByTagName("staerke").item(0).setTextContent(Integer.toString(spieler.getStaerke()));
						if(spieler.getTorschuss() != -1) element.getElementsByTagName("torschuss").item(0).setTextContent(Integer.toString(spieler.getTorschuss()));
						if(spieler.getMotivation() != -1) element.getElementsByTagName("motivation").item(0).setTextContent(Integer.toString(spieler.getMotivation()));
						if(spieler.getTore() != -1) element.getElementsByTagName("tore").item(0).setTextContent(Integer.toString(spieler.getTore()));
					}
				}
			}
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(XmlParser.filename));
			transformer.transform(source, result);
			System.out.println("File Update Done!");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void update(ISpieler spieler, int mannschaftId) {
		
		try 
		{
			NodeList nList = doc.getElementsByTagName("spieler");
		
			for(int temp = 0; temp < nList.getLength(); temp++)
			{
				Node node = nList.item(temp);
				if(node.getNodeType() == Node.ELEMENT_NODE)
				{
					Element element = (Element) node;
					if(element.getElementsByTagName("name").item(0).getTextContent().equals(spieler.getName()))
					{
						element.setAttribute("mannschaft", Integer.toString(mannschaftId));
					}
				}
			}
	
		
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(XmlParser.filename));
			transformer.transform(source, result);
			System.out.println("File Update Done!");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
	}

	@Override
	public void delete(ISpieler spieler) {
		
		try 
		{
			NodeList nList = doc.getElementsByTagName("spieler");
			
			for(int temp = 0; temp < nList.getLength(); temp++)
			{
				Node node = nList.item(temp);
				if(node.getNodeType() == Node.ELEMENT_NODE)
				{
					Element element = (Element) node;
					if(element.getElementsByTagName("name").item(0).getTextContent().equals(spieler.getName()))
					{
						element.getParentNode().removeChild(element);
					}
				}
			}
		
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(XmlParser.filename));
			transformer.transform(source, result);
			System.out.println("Node Deleted!");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		
	}
	
	public static void main(String[] args) throws DaoException 
	{
		//SpielerDaoXml test = new SpielerDaoXml();
		//ISpieler spieler = test.read(2);
		//System.out.println(spieler);
		//test.update(test.read(2));
		//ISpieler newSpieler = new Spieler("Max Kruse", 32, 10, 10, 10, 10);
		//test.create(newSpieler);
		//test.update(test.read(2), 1);
	}
}
