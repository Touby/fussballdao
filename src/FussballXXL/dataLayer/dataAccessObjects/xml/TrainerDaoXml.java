package FussballXXL.dataLayer.dataAccessObjects.xml;

import java.io.File;
import java.util.Scanner;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import FussballXXL.businessObjects.ITorwart;
import FussballXXL.businessObjects.ITrainer;
import FussballXXL.dataLayer.businessObjects.Trainer;
import FussballXXL.dataLayer.dataAccessObjects.ITrainerDao;
import FussballXXL.exceptions.DaoException;

public class TrainerDaoXml implements ITrainerDao {

	Document doc = XmlParser.parseXmlFile();
	
	public TrainerDaoXml()
	{
		
	}

	
	@Override
	public void create(ITrainer trainer) {
		
		try 
		{
			//find out the latest id so the id of the new trainer can be set 1 higher
			int lastId = 0; 
			NodeList nTrainerList = doc.getElementsByTagName("trainer");
			 
			for(int temp = 0; temp < nTrainerList.getLength(); temp++)
			{
				Node node = nTrainerList.item(temp);
				if(node.getNodeType() == Node.ELEMENT_NODE)
				{
					Element element = (Element) node;
					int id = Integer.parseInt(element.getAttribute("id"));
					lastId = id;
				}
			}
			

			//create new trainer
			NodeList nList = doc.getElementsByTagName("trainer");
			Node parentNode = nList.item(0).getParentNode();
			
			Element newTrainer = doc.createElement("trainer");
			newTrainer.setAttribute("id", Integer.toString(lastId + 1));
			
			Element newName = doc.createElement("name");
			newName.setTextContent(trainer.getName());
			
			Element newAlter = doc.createElement("alter");
			newAlter.setTextContent(Integer.toString(trainer.getAlter()));
			
			Element newErfahrung = doc.createElement("erfahrung");
			newErfahrung.setTextContent(Integer.toString(trainer.getErfahrung()));

			
			newTrainer.appendChild(newName);
			newTrainer.appendChild(newAlter);
			newTrainer.appendChild(newErfahrung);
			parentNode.appendChild(newTrainer);
			
			
			//remove every blank line so they can be added later with constant distance
			XPathFactory xfact = XPathFactory.newInstance();
			XPath xpath = xfact.newXPath();
			NodeList empty = (NodeList)xpath.evaluate("//text()[normalize-space(.) = '']", doc, XPathConstants.NODESET);
			
			for(int i = 0; i < empty.getLength(); i++)
			{
				Node node = empty.item(i);
				node.getParentNode().removeChild(node);
			}
		
			
			//transform xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(XmlParser.filename));
			transformer.transform(source, result);
			System.out.println("Node Created!");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public ITrainer read(int id) {

		 Element trainer = null;
		 
		 NodeList nList = doc.getElementsByTagName("trainer");
		 
		 for(int temp = 0; temp < nList.getLength(); temp++)
		 {
			 Node node = nList.item(temp);
			 if(node.getNodeType() == Node.ELEMENT_NODE)
			 {
				 Element element = (Element) node;
				 if(Integer.parseInt(element.getAttribute("id")) == id)
				 {
				 	trainer = element;
				 	break;
				 }
			 }
		 }
		 Trainer trainer1 = new Trainer(
				 trainer.getElementsByTagName("name").item(0).getTextContent(),
				 Integer.parseInt(trainer.getElementsByTagName("alter").item(0).getTextContent()),
				 Integer.parseInt(trainer.getElementsByTagName("erfahrung").item(0).getTextContent())
				 );
		 trainer1.setId(Integer.parseInt(trainer.getAttribute("id")));
		 return trainer1;
	}

	@Override
	public void update(ITrainer trainer) {
		
		Scanner scanner = new Scanner(System.in);
		Scanner scanner2 = new Scanner(System.in);
		int updateChoice;
		
		try 
		{
			NodeList nList = doc.getElementsByTagName("trainer");
		
			for(int temp = 0; temp < nList.getLength(); temp++)
			{
				Node node = nList.item(temp);
				if(node.getNodeType() == Node.ELEMENT_NODE)
				{
					Element element = (Element) node;
					if(element.getElementsByTagName("name").item(0).getTextContent().equals(trainer.getName()))
					{
						System.out.println("Update Trainer:\n1: Update Name\n2: Update Alter\n3: Update Erfahrung");
						updateChoice = scanner.nextInt();
						switch(updateChoice) 
						{
							case 1:
								System.out.println("Update Name: ");
								element.getElementsByTagName("name").item(0).setTextContent(scanner2.nextLine());
								break;
							case 2:
								System.out.println("Update Alter: ");
								element.getElementsByTagName("alter").item(0).setTextContent(scanner2.nextLine());
								break;
							case 3:
								System.out.println("Update Erfahrung: ");
								element.getElementsByTagName("erfahrung").item(0).setTextContent(scanner2.nextLine());
								break;
							default:
								System.out.println("Nichts");
								break;
						}
					}
				}
			}
			
			scanner.close();
			scanner2.close();
		
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(XmlParser.filename));
			transformer.transform(source, result);
			System.out.println("File Update Done!");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
	}

	@Override
	public void delete(ITrainer trainer) {
		
		try 
		{
			NodeList nList = doc.getElementsByTagName("trainer");
			
			for(int temp = 0; temp < nList.getLength(); temp++)
			{
				Node node = nList.item(temp);
				if(node.getNodeType() == Node.ELEMENT_NODE)
				{
					Element element = (Element) node;
					if(element.getElementsByTagName("name").item(0).getTextContent().equals(trainer.getName()))
					{
						element.getParentNode().removeChild(element);
					}
				}
			}
		
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(XmlParser.filename));
			transformer.transform(source, result);
			System.out.println("Node Deleted!");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		
	}
	
	public static void main(String[] args) throws DaoException 
	{
		TrainerDaoXml test = new TrainerDaoXml();
		ITrainer trainer = test.read(1);
		System.out.println(trainer);
		//test.update(test.read(1));
		//test.delete(test.read(1));
		ITrainer trainer1 = new Trainer("Florian Kohfeldt", 37, 7);
		test.create(trainer1);
	}
}
