package FussballXXL.dataLayer.dataAccessObjects.xml;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;


public class XmlParser {

	static String filename = "fussball.xml";
	
	public static Document parseXmlFile() {
		Document doc = null;

		try 
		{
			File inputFile = new File(filename);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();
	    }
			catch (Exception e)
		{
	        e.printStackTrace();
	    }
		
		return doc;
	}
}