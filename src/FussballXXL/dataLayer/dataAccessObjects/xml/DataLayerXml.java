package FussballXXL.dataLayer.dataAccessObjects.xml;
import FussballXXL.dataLayer.dataAccessObjects.IDataLayer;
import FussballXXL.dataLayer.dataAccessObjects.IMannschaftDao;
import FussballXXL.dataLayer.dataAccessObjects.ISpielerDao;
import FussballXXL.dataLayer.dataAccessObjects.ITorwartDao;
import FussballXXL.dataLayer.dataAccessObjects.ITrainerDao;

public class DataLayerXml implements IDataLayer {
	
	IMannschaftDao mannschaftDao = new MannschaftDaoXml();
	ITrainerDao trainerDao = new TrainerDaoXml();
	ITorwartDao torwartDao = new TorwartDaoXml();
	ISpielerDao spielerDao = new SpielerDaoXml();

	@Override
	public IMannschaftDao getMannschaftDao() {

		return mannschaftDao;
	}

	@Override
	public ITrainerDao getTrainerDao() {
		
		return trainerDao;
	}

	@Override
	public ITorwartDao getTorwartDao() {
		
		return torwartDao;
	}

	@Override
	public ISpielerDao getSpielerDao() {
		
		return spielerDao;
	}
}