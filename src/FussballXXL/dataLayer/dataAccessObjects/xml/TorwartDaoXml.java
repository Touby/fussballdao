package FussballXXL.dataLayer.dataAccessObjects.xml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import FussballXXL.businessObjects.ITorwart;
import FussballXXL.dataLayer.businessObjects.Torwart;
import FussballXXL.dataLayer.dataAccessObjects.ITorwartDao;
import FussballXXL.exceptions.DaoException;

import java.io.File;
import java.util.Scanner;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPath;


public class TorwartDaoXml implements ITorwartDao {
	
	
	Document doc = XmlParser.parseXmlFile();
	
	public TorwartDaoXml()
	{
		
	}

	@Override
	public void create(ITorwart torwart) {
		
		try 
		{
			//find out the latest id so the id of the new torwart can be set 1 higher
			int lastId = 0; 
			NodeList nTorwartList = doc.getElementsByTagName("torwart");
			 
			for(int temp = 0; temp < nTorwartList.getLength(); temp++)
			{
				Node node = nTorwartList.item(temp);
				if(node.getNodeType() == Node.ELEMENT_NODE)
				{
					Element element = (Element) node;
					int id = Integer.parseInt(element.getAttribute("id"));
					lastId = id;
				}
			}
			

			//create new torwart
			NodeList nList = doc.getElementsByTagName("torwart");
			Node parentNode = nList.item(0).getParentNode();
			
			Element newTorwart = doc.createElement("torwart");
			newTorwart.setAttribute("id", Integer.toString(lastId + 1));
			
			Element newName = doc.createElement("name");
			newName.setTextContent(torwart.getName());
			
			Element newAlter = doc.createElement("alter");
			newAlter.setTextContent(Integer.toString(torwart.getAlter()));
			
			Element newMotivation = doc.createElement("motivation");
			newMotivation.setTextContent(Integer.toString(torwart.getMotivation()));
			
			Element newStaerke = doc.createElement("staerke");
			newStaerke.setTextContent(Integer.toString(torwart.getStaerke()));
			
			Element newTore = doc.createElement("tore");
			newTore.setTextContent(Integer.toString(torwart.getTore()));
			
			Element newTorschuss = doc.createElement("torschuss");
			newTorschuss.setTextContent(Integer.toString(torwart.getTorschuss()));
			
			Element newReaktion = doc.createElement("reaktion");
			newReaktion.setTextContent(Integer.toString(torwart.getReaktion()));
			
			newTorwart.appendChild(newName);
			newTorwart.appendChild(newAlter);
			newTorwart.appendChild(newMotivation);
			newTorwart.appendChild(newStaerke);
			newTorwart.appendChild(newTore);
			newTorwart.appendChild(newTorschuss);
			newTorwart.appendChild(newReaktion);
			parentNode.appendChild(newTorwart);
			
			
			//remove every blank line so they can be added later with constant distance
			XPathFactory xfact = XPathFactory.newInstance();
			XPath xpath = xfact.newXPath();
			NodeList empty = (NodeList)xpath.evaluate("//text()[normalize-space(.) = '']", doc, XPathConstants.NODESET);
			
			for(int i = 0; i < empty.getLength(); i++)
			{
				Node node = empty.item(i);
				node.getParentNode().removeChild(node);
			}
		
			
			//transform xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(XmlParser.filename));
			transformer.transform(source, result);
			System.out.println("Node Created!");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	@Override
	public ITorwart read(int id) {
		
		 Element torwart = null;
		 
		 NodeList nList = doc.getElementsByTagName("torwart");
		 
		 for(int temp = 0; temp < nList.getLength(); temp++)
		 {
			 Node node = nList.item(temp);
			 if(node.getNodeType() == Node.ELEMENT_NODE)
			 {
				 Element element = (Element) node;
				 if(Integer.parseInt(element.getAttribute("id")) == id)
				 {
				 	torwart = element;
				 	break;
				 }
			 }
		 }
		 Torwart torwart1 = new Torwart(
				 torwart.getElementsByTagName("name").item(0).getTextContent(),
				 Integer.parseInt(torwart.getElementsByTagName("alter").item(0).getTextContent()),
				 Integer.parseInt(torwart.getElementsByTagName("staerke").item(0).getTextContent()),
				 Integer.parseInt(torwart.getElementsByTagName("motivation").item(0).getTextContent()),
				 Integer.parseInt(torwart.getElementsByTagName("reaktion").item(0).getTextContent())
				 );
		 torwart1.setId(Integer.parseInt(torwart.getAttribute("id")));
		 return torwart1;
	}

	@Override
	public void update(ITorwart torwart) {
		
		Scanner scanner = new Scanner(System.in);
		Scanner scanner2 = new Scanner(System.in);
		int updateChoice;
		
		try 
		{
			NodeList nList = doc.getElementsByTagName("torwart");
		
			for(int temp = 0; temp < nList.getLength(); temp++)
			{
				Node node = nList.item(temp);
				if(node.getNodeType() == Node.ELEMENT_NODE)
				{
					Element element = (Element) node;
					if(element.getElementsByTagName("name").item(0).getTextContent().equals(torwart.getName()))
					{
						System.out.println("Update Torwart:\n1: Update Name\n2: Update Alter\n3: Update Staerke\n4: Update Motivation\n5: Update Reaktion");
						updateChoice = scanner.nextInt();
						switch(updateChoice) 
						{
							case 1:
								System.out.println("Update Name: ");
								element.getElementsByTagName("name").item(0).setTextContent(scanner2.nextLine());
								break;
							case 2:
								System.out.println("Update Alter: ");
								element.getElementsByTagName("alter").item(0).setTextContent(scanner2.nextLine());
								break;
							case 3:
								System.out.println("Update Staerke: ");
								element.getElementsByTagName("staerke").item(0).setTextContent(scanner2.nextLine());
								break;
							case 4:
								System.out.println("Update Motivation: ");
								element.getElementsByTagName("motivation").item(0).setTextContent(scanner2.nextLine());
								break;
							case 5:
								System.out.println("Update Reaktion5: ");
								element.getElementsByTagName("reaktion").item(0).setTextContent(scanner2.nextLine());
								break;
							default:
								System.out.println("Nichts");
								break;
						}
					}
				}
			}
			
			scanner.close();
			scanner2.close();
		
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(XmlParser.filename));
			transformer.transform(source, result);
			System.out.println("File Update Done!");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
	}

	@Override
	public void delete(ITorwart torwart) {
		
		try 
		{
			NodeList nList = doc.getElementsByTagName("torwart");
			
			for(int temp = 0; temp < nList.getLength(); temp++)
			{
				Node node = nList.item(temp);
				if(node.getNodeType() == Node.ELEMENT_NODE)
				{
					Element element = (Element) node;
					if(element.getElementsByTagName("name").item(0).getTextContent().equals(torwart.getName()))
					{
						element.getParentNode().removeChild(element);
					}
				}
			}
		
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(XmlParser.filename));
			transformer.transform(source, result);
			System.out.println("Node Deleted!");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) throws DaoException 
	{
		TorwartDaoXml test = new TorwartDaoXml();
		ITorwart torwart = test.read(1);
		System.out.println(torwart);
		//test.update(test.read(2));
		//test.delete(test.read(1));
		ITorwart newTorwart = new Torwart("Frank Rost", 40, 5, 5, 5);
		test.create(newTorwart);
	}
}
