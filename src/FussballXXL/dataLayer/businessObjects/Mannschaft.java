package FussballXXL.dataLayer.businessObjects;

import java.util.ArrayList;

import FussballXXL.businessObjects.*;


public class Mannschaft implements IMannschaft {
	
	private int id;
	private String name;
	private ITrainer trainer;
	private ITorwart torwart;
	private ArrayList<ISpieler> spielerListe;

	/**
	 * Konstruktor.
	 * @param name Der Name.
	 * @param trainer Der Trainer.
	 * @param torwart Der Torwart.
	 * @param spielerListe Die Feldspielerliste.
	 */
	public Mannschaft(String name, ITrainer trainer, ITorwart torwart, ArrayList<ISpieler> spielerListe) {
		this.name = name;
		this.trainer = trainer;
		this.torwart = torwart;
		this.spielerListe = spielerListe;
	}

	/**
	 * Ruft den Namen ab.
	 * @return Der Name.
	 */
	
	public int getId()
	{
		return this.id;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}

	/**
	 * �ndert den Namen.
	 * @param name Der neue Name.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Ruft den Trainer ab.
	 * @return Der Trainer.
	 */
	public ITrainer getTrainer() {
		return this.trainer;
	}

	/**
	 * �ndert den Trainer.
	 * @param trainer Der neue Trainer.
	 */
	public void setTrainer(ITrainer trainer) {
		this.trainer = trainer;
	}

	/**
	 * Ruft den Torwart ab.
	 * @return Der Torwart.
	 */
	public ITorwart getTorwart() {
		return this.torwart;
	}

	/**
	 * �ndert den Torwart.
	 * @param torwart Der neue Torwart.
	 */
	public void setTorwart(ITorwart torwart) {
		this.torwart = torwart;
	}

	/**
	 * Ruft die Feldspielerliste ab.
	 * @return Die Feldspielerliste.
	 */
	public ArrayList<ISpieler> getSpielerListe() {
		return this.spielerListe;
	}

	/**
	 * �ndert die Feldspielerliste.
	 * @param spielerListe Die neue Feldspielerliste.
	 */
	public void setSpielerListe(ArrayList<ISpieler> spielerListe) {
		this.spielerListe = spielerListe;
	}
	
	/**
	 * Errechnet die Motivation der Mannschaft als Mittelwert der Motivation aller Einzelspieler.
	 * Werte von 1 (schlecht) bis 10 (super) sind m�glich.
	 * @return Die Motivation.
	 */
	public int getMotivation() {
		int motivation = torwart.getMotivation();
		for (ISpieler spieler : spielerListe) {
			motivation = motivation + spieler.getMotivation();
		}
		motivation = motivation / (spielerListe.size() + 1);
		return motivation;
	}
	
	/**
	 * Errechnet die Spielst�rke der Mannschaft als Mittelwert der Spielst�rke aller Einzelspieler.
	 * Werte von 1 (schlecht) bis 10 (super) sind m�glich.
	 * @return Die Spielst�rke.
	 */
	public int getStaerke() {
		int staerke = torwart.getStaerke();
		for (ISpieler spieler : spielerListe) {
			staerke = staerke + spieler.getStaerke();
		}
		staerke = staerke / (spielerListe.size() + 1);
		return staerke;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String text = "Mannschaft: " + name;
		text = text + "\n" + trainer;
		text = text + "\n" + torwart;
		for (ISpieler spieler : spielerListe) {
			text = text + "\n" + spieler;
		}
		return text;
	}

}
