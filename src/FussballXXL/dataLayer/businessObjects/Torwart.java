package FussballXXL.dataLayer.businessObjects;

import FussballXXL.businessObjects.ITorwart;

public class Torwart extends Spieler implements ITorwart {

	private int reaktion;

	/**
	 * Konstruktor
	 * @param name Der Name.
	 * @param alter Das Alter.
	 * @param staerke Die Spielst�rke. Werte von 1 (schlecht) bis 10 (super) sind m�glich.
	 * @param motivation Die Motivation. Werte von 1 (schlecht) bis 10 (super) sind m�glich.
	 * @param reaktion Die Reaktion. Werte von 1 (schlecht) bis 10 (super) sind m�glich.
	 */
	public Torwart(String name, int alter, int staerke, int motivation, int reaktion) {
		super(name, alter, staerke, 0, motivation, 0);
		this.reaktion = reaktion;
	}

	/**
	 * Ruft den Wert f�r die Reaktion ab.
	 * Werte von 1 (schlecht) bis 10 (super) sind m�glich.
	 * @return Der Wert f�r die Reaktion.
	 */
	public int getReaktion() {
		return reaktion;
	}

	/**
	 * �ndert den Wert f�r die Reaktion.
	 * Werte von 1 (schlecht) bis 10 (super) sind m�glich.
	 * @param reaktion Der neue Wert f�r die Reaktion.
	 */
	public void setReaktion(int reaktion) {
		this.reaktion = reaktion;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String text = "\n\tTorwart-" + super.toString();
		text = text + "\n\t" + "Reaktion: " + reaktion;
		return text;
	}

}
