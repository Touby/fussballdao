package FussballXXL.dataLayer.businessObjects;

import FussballXXL.businessObjects.IPerson;

public abstract class Person implements IPerson {

	private int id;
	private String name;
	private int alter;

	/**
	 * Konstruktor
	 * @param name Der Name.
	 * @param alter Das Alter.
	 */
	public Person(String name, int alter) {
		this.name = name;
		this.alter = alter;
	}

	public int getId()
	{
		return this.id;
	}
	
	public void setId(int id) 
	{
		this.id = id;
	}
	
	/**
	 * Ruft den Namen ab.
	 * @return Der Name.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * �ndert den Namen.
	 * @param name Der neue Name.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Ruft das Alter ab.
	 * @return Das Alter.
	 */
	public int getAlter() {
		return this.alter;
	}

	/**
	 * �ndert das Alter.
	 * @param alter Das neue Alter.
	 */
	public void setAlter(int alter) {
		this.alter = alter;
	}
	
	@Override
	public String toString()
	{
		String text = "Id: " + getId();
		text = text + "\n\t" + "Name: " + getName();
		text = text + "\n\t" + "Alter: " + getAlter();
		return text;
	}

}
