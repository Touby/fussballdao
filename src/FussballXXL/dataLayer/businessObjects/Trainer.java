package FussballXXL.dataLayer.businessObjects;

import FussballXXL.businessObjects.ITrainer;

public class Trainer extends Person implements ITrainer {

	private int erfahrung;

	/**
	 * Konstruktor
	 * @param name Der Name.
	 * @param alter Das Alter.
	 * @param erfahrung Der Wert f�r die Erfahrung. Werte von 1 (unerfahren) bis 10 (Trainerfuchs) sind m�glich.
	 */
	public Trainer(String name, int alter, int erfahrung) {
		super(name, alter);
		this.erfahrung = erfahrung;
	}

	/**
	 * Ruft den Wert f�r die Erfahrung ab.
	 * Werte von 1 (unerfahren) bis 10 (Trainerfuchs) sind m�glich.
	 * @return Der Wert f�r die Erfahrung.
	 */
	public int getErfahrung() {
		return this.erfahrung;
	}

	/**
	 * �ndert den Wert f�r die Erfahrung.
	 * Werte von 1 (unerfahren) bis 10 (Trainerfuchs) sind m�glich.
	 * @param erfahrung Der neue Wert f�r die Erfahrung.
	 */
	public void setErfahrung(int erfahrung) {
		this.erfahrung = erfahrung;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String text = "\n\tTrainer" + super.toString();
		text = text + "\n\t" + "Erfahrung: " + erfahrung;
		return text;
	}

}
