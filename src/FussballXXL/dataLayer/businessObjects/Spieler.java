package FussballXXL.dataLayer.businessObjects;

import FussballXXL.businessObjects.ISpieler;

public class Spieler extends Person implements ISpieler {

	private int staerke;
	private int torschuss;
	private int motivation;
	private int tore;

	/**
	 * Konstruktor
	 * @param name Der Name.
	 * @param alter Das Alter.
	 * @param staerke Die Spielst�rke. Werte von 1 (schlecht) bis 10 (super) sind m�glich.
	 * @param torschuss Die Treffsicherheit beim Torschuss. Werte von 1 (schlecht) bis 10 (super) sind m�glich.
	 * @param motivation Die Motivation. Werte von 1 (schlecht) bis 10 (super) sind m�glich.
	 * @param tore Die Anzahl der geschossenen Tore.
	 */
	public Spieler(String name, int alter, int staerke, int torschuss, int motivation, int tore) {
		super(name, alter);
		this.staerke = staerke;
		this.torschuss = torschuss;
		this.motivation = motivation;
		this.tore = tore;
	}

	/**
	 * Ruft die Spielst�rke ab.
	 * Werte von 1 (schlecht) bis 10 (super) sind m�glich.
	 * @return Die Spielst�rke.
	 */
	public int getStaerke() {
		return this.staerke;
	}

	/**
	 * �ndert die Spielst�rke.
	 * Werte von 1 (schlecht) bis 10 (super) sind m�glich.
	 * @param staerke Die neue Spielst�rke.
	 */
	public void setStaerke(int staerke) {
		this.staerke = staerke;
	}

	/**
	 * Ruft die Treffsicherheit beim Torschuss ab.
	 * Werte von 1 (schlecht) bis 10 (super) sind m�glich.
	 * @return Die Treffsicherheit beim Torschuss.
	 */
	public int getTorschuss() {
		return this.torschuss;
	}

	/**
	 * �ndert die Treffsicherheit beim Torschuss.
	 * Werte von 1 (schlecht) bis 10 (super) sind m�glich.
	 * @param torschuss Die neue Treffsicherheit beim Torschuss.
	 */
	public void setTorschuss(int torschuss) {
		this.torschuss = torschuss;
	}

	/**
	 * Ruft den Wert f�r die Motivation ab.
	 * Werte von 1 (schlecht) bis 10 (super) sind m�glich.
	 * @return Der Wert f�r die Motivation.
	 */
	public int getMotivation() {
		return this.motivation;
	}

	/**
	 * �ndert den Wert f�r die Motivation.
	 * Werte von 1 (schlecht) bis 10 (super) sind m�glich.
	 * @param motivation Der neue Wert f�r die Motivation.
	 */
	public void setMotivation(int motivation) {
		this.motivation = motivation;
	}

	/**
	 * Ruft die Anzahl der geschossenen Tore ab.
	 * @return Die Anzahl der geschossenen Tore.
	 */
	public int getTore() {
		return this.tore;
	}

	/**
	 * �ndert die Anzahl der geschossenen Tore.
	 * @param tore Die Anzahl der geschossenen Tore.
	 */
	public void setTore(int tore) {
		this.tore = tore;
	}

	/**
	 * F�gt ein Tor hinzu.
	 */
	public void addTor() {
		tore++;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String text = super.toString();
		text = text + "\n\t" + "Spielst�rke: " + this.staerke;
		text = text + "\n\t" + "Treffsicherheit: " + this.torschuss;
		text = text + "\n\t" + "Motivation: " + this.motivation;
		text = text + "\n\t" + "Tore: " + this.tore;
		return text;
	}

}
