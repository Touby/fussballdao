package FussballXXL.exceptions;

public class DaoException extends Exception {
	
	private String message;
	
	/**
	 * 
	 * @param message Die Exception-message
	 */
	public DaoException(String message)
	{
		super();
		this.message = message;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage()
	{
		return this.message;
	}
}
