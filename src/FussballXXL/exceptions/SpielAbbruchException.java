package FussballXXL.exceptions;

public class SpielAbbruchException extends Exception {
	
	private int spielMinute;
	
	/**
	 * Konstruktor.
	 * @param spielMinute Die Spielminute.
	 */
	public SpielAbbruchException(int spielMinute) {
		super();
		this.spielMinute = spielMinute;
	}

	/**
	 * Ruft die Spielminute ab.
	 * @return Die Spielminute.
	 */
	public int getSpielMinute() {
		return spielMinute;
	}

	/* (non-Javadoc)
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage() {
		return "Spielabbruch in Minute " + spielMinute;
	}
	

}
