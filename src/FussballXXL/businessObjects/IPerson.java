package FussballXXL.businessObjects;

public interface IPerson {
	public int getId();
	public String getName();
	public void setName(String name);
	public int getAlter();
	public void setAlter(int alter);
}
