package FussballXXL.businessObjects;

public class Ergebnis {
	
	private int toreHeim;
	private int toreGast;
	
	/**
	 * Ruft die Tore der Heimmannschaft ab.
	 * @return Die Tore der Heimmannschaft.
	 */
	public int getToreHeim() {
		return toreHeim;
	}

	/**
	 * Ruft die Tore der Gastmannschaft ab.
	 * @return Die Tore der Gastmannschaft.
	 */
	public int getToreGast() {
		return toreGast;
	}

	/**
	 * Erh�ht die Toranzahl f�r die Gastmannschaft.
	 */
	public void addToreHeim() {
		toreHeim = toreHeim + 1;
	}
	
	/**
	 * Erh�ht die Toranzahl f�r die Gastmannschaft.
	 */
	public void addToreGast() {
		toreGast = toreGast + 1;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return toreHeim + ":" + toreGast	+ "\n";
	}

}
