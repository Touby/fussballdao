package FussballXXL.businessObjects;

import FussballXXL.dataLayer.businessObjects.Mannschaft;

public class Spiel {

	private IMannschaft heim;
	private IMannschaft gast;
	private Ergebnis ergebnis;
	private StringBuilder spielbericht;

	/**
	 * Konstruktor.
	 * @param heim Die Heimmannschaft.
	 * @param gast Die Gastmannschaft.
	 */
	public Spiel(IMannschaft heim, IMannschaft gast) {
		this.heim = heim;
		this.gast = gast;
		ergebnis = new Ergebnis();
		spielbericht = new StringBuilder();
	}

	/**
	 * Ruft die Heimmannschaft ab.
	 * @return Die Heimmannschaft.
	 */
	public IMannschaft getHeim() {
		return heim;
	}

	/**
	 * Ruft die Gastmannschaft ab.
	 * @return Die Gastmannschaft.
	 */
	public IMannschaft getGast() {
		return gast;
	}
	
	/**
	 * Ruft das Ergebnis ab.
	 * @return Das Ergebnis.
	 */
	public Ergebnis getErgebnis() {
		return ergebnis;
	}
	
	/**
	 * Ruft den Spielbericht ab.
	 * @return Den Spielbericht.
	 */
	public StringBuilder getSpielbericht() {
		return spielbericht;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String text = "Spiel zwischen \"" + heim.getName() + "\" und \"" + gast.getName() + "\":\n";
		text = text + spielbericht;
		return text;
	}

}
