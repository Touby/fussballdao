package FussballXXL.businessObjects;

public interface ISpieler extends IPerson {
	public int getStaerke();
	public void setStaerke(int staerke);
	public int getTorschuss();
	public void setTorschuss(int torschuss);
	public int getMotivation();
	public void setMotivation(int motivation);
	public int getTore();
	public void setTore(int tore);
	public void addTor();
}
