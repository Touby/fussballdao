package FussballXXL.businessObjects;

public interface ITrainer extends IPerson {
	public int getErfahrung();
	public void setErfahrung(int erfahrung);
}
