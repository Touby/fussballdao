package FussballXXL.businessObjects;
import java.util.ArrayList;

public interface IMannschaft {
	public int getId();
	public String getName();
	public void setName(String name);
	public ITrainer getTrainer();
	public void setTrainer(ITrainer trainer);
	public ITorwart getTorwart();
	public void setTorwart(ITorwart torwart);
	public ArrayList<ISpieler> getSpielerListe();
	public void setSpielerListe(ArrayList<ISpieler> spielerListe);
	public int getStaerke();
	public int getMotivation();
	
}
