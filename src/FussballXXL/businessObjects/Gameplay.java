package FussballXXL.businessObjects;

import java.util.Random;

import FussballXXL.exceptions.SpielAbbruchException;

public class Gameplay {
	
	private static final int SPIELZEIT = 90;
	private static final int MAX_NACHSPIELZEIT = 5;
	private static final int MAX_DAUER_BIS_AKTION = 10;
	
	/**
	 * Simuliert ein Spiel.
	 * @param spiel Das zu spielende Spiel.
	 * @throws SpielAbbruchException 
	 */
	
	public static void spielen(Spiel spiel) throws SpielAbbruchException {
		// spielbericht = new StringBuilder();
		Random random = new Random();
		IMannschaft offensiv;
		IMannschaft defensiv;
		// Zuf�llige Spieldauer festlegen
		int spielDauer = SPIELZEIT + random.nextInt(MAX_NACHSPIELZEIT + 1);
		// Zuf�llige Spielminute f�r erste Aktion festlegen
		int spielMinute = 1 + random.nextInt(MAX_DAUER_BIS_AKTION + 1);
		do {
			// Spielabbruch �berpr�fen
			if (brecheSpielAb()) {
				throw new SpielAbbruchException(spielMinute);
			}
			// Ermittlung der offensiven bzw. defensiven Mannschaft
			int heimWert = ermittelMannschaftsWert(spiel.getHeim());
			int gastWert = ermittelMannschaftsWert(spiel.getGast());
			int summe = heimWert + gastWert;
			int zufall = random.nextInt(Math.round(summe + 1));
			if (zufall <= heimWert) {
				offensiv = spiel.getHeim();
				defensiv = spiel.getGast();
			}
			else {
				offensiv = spiel.getGast();
				defensiv = spiel.getHeim();
			}
			// Sch�tze bestimmen
			int schuetzeNr = random.nextInt(offensiv.getSpielerListe().size());
			ISpieler schuetze = offensiv.getSpielerListe().get(schuetzeNr);
			// Ermitteln ob Torschuss erfolgreich oder nicht
			boolean getroffen = erzieltTor(schuetze, defensiv.getTorwart());
			if (getroffen) {
				schuetze.addTor();
				if (offensiv == spiel.getHeim()) {
					spiel.getErgebnis().addToreHeim();
				}
				else {
					spiel.getErgebnis().addToreGast();
				}
				spiel.getSpielbericht().append(spielMinute + ": Tor von " + schuetze.getName() + "!\n");
			}
			else {
				spiel.getSpielbericht().append(spielMinute + ": Schuss von " + schuetze.getName() + " gehalten.\n");				
			}
			// Zuf�llige Spielminute f�r n�chste Aktion festlegen
			spielMinute = spielMinute + random.nextInt(MAX_DAUER_BIS_AKTION + 1);
		}while(spielMinute <= spielDauer);
		spiel.getSpielbericht();//.append(spiel.getErgebnis());
	}
	
	/**
	 * Errechnet den aktuellen St�rkewert eine Mannschaft. 
	 * Errechnet sich aus der Spielst�rke und Motivation der Mannschaft und der Erfahrung des Trainers. 
	 * @return Der St�rkewert.
	 */
	private static int ermittelMannschaftsWert(IMannschaft mannschaft) {
		Random random = new Random();
		int wert = mannschaft.getStaerke() * mannschaft.getMotivation() * mannschaft.getTrainer().getErfahrung();
		wert = Math.max(1, wert);
		return wert;
	}
	
	/**
	 * Ermittelt, ob ein Torschuss erfolgreich ist oder nicht.
	 * @param schuetze Der Torsch�tze.
	 * @param torwart Der Torwart.
	 * @return Ja, wenn der Torschuss erfolgreich ist. Sonst nein.
	 */
	private static boolean erzieltTor(ISpieler schuetze, ITorwart torwart) {
		boolean getroffen;
		Random random = new Random();
		// St�rke des Schusses ermitteln
		int schuss = schuetze.getTorschuss() + random.nextInt(5) - 2;
		schuss = Math.max(1, schuss);
		// St�rke der Abwehr ermitteln
		int abwehr = torwart.getReaktion() + random.nextInt(5) - 2;
		abwehr = Math.max(1, abwehr);  
		// Ermitteln, ob Sch�tze trifft
		if (schuss > abwehr) {
			getroffen = true;
		}
		else {
			getroffen = false;
		}
		return getroffen;
	}
	
	/**
	 * Ermittelt, ob ein Spiel abgebrochen wird oder nicht.
	 * @return Ja, wenn das Spiel abgebrochen wird. Sonst nein.
	 */
	private static boolean brecheSpielAb() {
		Random random = new Random();
		int wert1 = random.nextInt(1000);
		if (wert1 == 0) {
			return true;
		}
		return false;
	}

}
