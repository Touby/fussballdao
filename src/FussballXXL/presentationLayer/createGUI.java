package FussballXXL.presentationLayer;

import FussballXXL.dataLayer.dataAccessObjects.xml.*;
import FussballXXL.exceptions.DaoException;
import FussballXXL.exceptions.SpielAbbruchException;
import FussballXXL.dataLayer.businessObjects.Spieler;
import FussballXXL.dataLayer.businessObjects.Torwart;
import FussballXXL.dataLayer.businessObjects.Trainer;
import FussballXXL.dataLayer.dataAccessObjects.sqlite.*;
import FussballXXL.businessObjects.*;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.w3c.dom.NodeList;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.Window.Type;
import javax.swing.JTabbedPane;
import javax.swing.ImageIcon;
import java.awt.GridLayout;
import java.awt.Image;

import javax.swing.JComboBox;
import java.awt.GridBagLayout;
import javax.swing.JCheckBox;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.List;

import javax.swing.JTextField;
import java.awt.Button;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import javax.swing.JEditorPane;
import javax.swing.JTextArea;
import java.awt.Component;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DropMode;
import javax.swing.JScrollPane;
import java.awt.Font;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class createGUI extends JFrame 
{
	private JPanel contentPane;
	private JTextField nameText;
	private String name = "";
	private JTextField alterText;
	private int alter = -1;
	private JTextField motivationText;
	private int motivation = -1;
	private JTextField staerkeText;
	private int staerke = -1;
	private JTextField torschussText;
	private int torschuss = -1;
	private JTextField toreText;
	private int tore = -1;
	private JComboBox<String> teamDropdown1;
	private JComboBox<String> teamDropdown2;
	private JComboBox<String> mannschaftDropdownDB;
	private JComboBox<String> datenbankDropdown;
	private JComboBox<String> rolleDropdownDB;
	private String selectedMannschaftDropdownDB;
	private static DataLayerXml dataLayerXml = new DataLayerXml();
	private static DataLayerSqlite dataLayerSqlite = new DataLayerSqlite();
	private IMannschaft mannschaft1 = null;
	private IMannschaft mannschaft2 = null;
	private StringBuilder spielbericht;
	private JTextArea verlaufText;
	private String dataLayer = "Xml";
	private String selectedRolle;
	
	private org.w3c.dom.Document doc = XmlParser.parseXmlFile();
	private NodeList mannschaftList = doc.getElementsByTagName("mannschaft");
	private NodeList spielerList = doc.getElementsByTagName("spieler");
	private NodeList trainerList = doc.getElementsByTagName("trainer");
	private NodeList torwartList = doc.getElementsByTagName("torwart");
	private JTextField reaktionText;
	private int reaktion = -1;
	private JTextField erfahrungText;
	private int erfahrung = -1;
	
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					createGUI frame = new createGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the frame.
	 * @throws DaoException 
	 */
	public createGUI() throws DaoException 
	{
    	setType(Type.POPUP);
		setTitle("Fu\u00DFballDao");
		setIconImage(Toolkit.getDefaultToolkit().getImage(createGUI.class.getResource("/FussballXXL/presentationLayer/fu\u00DFballDraw.png")));
		setBackground(new Color(255, 255, 255));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 204));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		
		JPanel Spiel = new JPanel();
		Spiel.setBackground(Color.WHITE);
		tabbedPane.addTab("Spiel", new ImageIcon(createGUI.class.getResource("/FussballXXL/presentationLayer/pfeifeDraw.png")), Spiel, "Hier kann das Spiel gestartet werden.");
		tabbedPane.setBackgroundAt(0, Color.WHITE);
		tabbedPane.setEnabledAt(0, true);
		GridBagLayout gbl_Spiel = new GridBagLayout();
		gbl_Spiel.columnWidths = new int[] {0, 30, 0};
		gbl_Spiel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_Spiel.columnWeights = new double[]{1.0, 0.0, 1.0};
		gbl_Spiel.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		Spiel.setLayout(gbl_Spiel);
		
		JPanel Datenbank = new JPanel();
		Datenbank.setBackground(Color.WHITE);
		tabbedPane.addTab("Datenbank", new ImageIcon(createGUI.class.getResource("/FussballXXL/presentationLayer/datenbankDraw.png")), Datenbank, "Hier k\u00F6nnen neue Datenbank Eintr\u00E4ge angelegt werden.");
		tabbedPane.setEnabledAt(1, true);
		GridBagLayout gbl_Datenbank = new GridBagLayout();
		gbl_Datenbank.columnWidths = new int[]{0, 0, 0, 0, 0, 0};
		gbl_Datenbank.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_Datenbank.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_Datenbank.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		Datenbank.setLayout(gbl_Datenbank);
		
		teamDropdown1 = new JComboBox<String>();
		teamDropdown1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{	
				String tmp = teamDropdown1.getSelectedItem().toString();
				for(int i = 1; i <= mannschaftList.getLength(); i++)
				{
					try {
						if(dataLayer.compareTo("Xml") == 0) if(tmp.compareTo(dataLayerXml.getMannschaftDao().read(i).getName()) == 0) mannschaft1 = dataLayerXml.getMannschaftDao().read(i);
						else if(tmp.compareTo(dataLayerSqlite.getMannschaftDao().read(i).getName()) == 0) mannschaft1 = dataLayerXml.getMannschaftDao().read(i);
					} catch (DaoException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		
		GridBagConstraints gbc_teamDropdown1 = new GridBagConstraints();
		gbc_teamDropdown1.gridwidth = 3;
		gbc_teamDropdown1.insets = new Insets(0, 0, 5, 0);
		gbc_teamDropdown1.fill = GridBagConstraints.HORIZONTAL;
		gbc_teamDropdown1.gridx = 0;
		gbc_teamDropdown1.gridy = 0;
		Spiel.add(teamDropdown1, gbc_teamDropdown1);
		teamDropdown1.addItem("");
		teamDropdown1.setSelectedItem("");
		
		teamDropdown2 = new JComboBox<String>();
		teamDropdown2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				String tmp = teamDropdown2.getSelectedItem().toString();
				for(int i = 1; i <= mannschaftList.getLength(); i++)
				{
					try {
						if(dataLayer.compareTo("Xml") == 0) if(tmp.compareTo(dataLayerXml.getMannschaftDao().read(i).getName()) == 0) mannschaft2 = dataLayerXml.getMannschaftDao().read(i);
						else if(tmp.compareTo(dataLayerSqlite.getMannschaftDao().read(i).getName()) == 0) mannschaft2 = dataLayerSqlite.getMannschaftDao().read(i);
					} catch (DaoException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		
		GridBagConstraints gbc_teamDropdown2 = new GridBagConstraints();
		gbc_teamDropdown2.gridwidth = 3;
		gbc_teamDropdown2.insets = new Insets(0, 0, 5, 0);
		gbc_teamDropdown2.fill = GridBagConstraints.HORIZONTAL;
		gbc_teamDropdown2.gridx = 0;
		gbc_teamDropdown2.gridy = 1;
		Spiel.add(teamDropdown2, gbc_teamDropdown2);
		teamDropdown2.addItem("");
		teamDropdown2.setSelectedItem("");
		
		mannschaftDropdownDB = new JComboBox<String>();
		mannschaftDropdownDB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				selectedMannschaftDropdownDB = mannschaftDropdownDB.getSelectedItem().toString();
			}
		});
		GridBagConstraints gbc_mannschaftDropdownDB = new GridBagConstraints();
		gbc_mannschaftDropdownDB.insets = new Insets(0, 0, 5, 5);
		gbc_mannschaftDropdownDB.fill = GridBagConstraints.HORIZONTAL;
		gbc_mannschaftDropdownDB.gridx = 1;
		gbc_mannschaftDropdownDB.gridy = 4;
		Datenbank.add(mannschaftDropdownDB, gbc_mannschaftDropdownDB);
		mannschaftDropdownDB.addItem("");
		mannschaftDropdownDB.setSelectedItem("");
		
		for(int i = 1; i <= mannschaftList.getLength(); i++)
		{
			if(dataLayer.compareTo("Xml") == 0)
			{
				teamDropdown1.addItem(dataLayerXml.getMannschaftDao().read(i).getName());
				teamDropdown2.addItem(dataLayerXml.getMannschaftDao().read(i).getName());
				mannschaftDropdownDB.addItem(dataLayerXml.getMannschaftDao().read(i).getName());
			}
			else
			{	
				teamDropdown1.addItem(dataLayerSqlite.getMannschaftDao().read(i).getName());
				teamDropdown2.addItem(dataLayerSqlite.getMannschaftDao().read(i).getName());
				mannschaftDropdownDB.addItem(dataLayerSqlite.getMannschaftDao().read(i).getName());
			}
			
		}
		
		JScrollPane scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridwidth = 3;
		gbc_scrollPane_1.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane_1.gridx = 0;
		gbc_scrollPane_1.gridy = 2;
		Spiel.add(scrollPane_1, gbc_scrollPane_1);
		
		JTextArea spielVerlaufText = new JTextArea();
		spielVerlaufText.setEditable(false);
		scrollPane_1.setViewportView(spielVerlaufText);
		
		JTextPane ergebnissText = new JTextPane();
		ergebnissText.setEditable(false);
		GridBagConstraints gbc_ergebnissText = new GridBagConstraints();
		gbc_ergebnissText.anchor = GridBagConstraints.WEST;
		gbc_ergebnissText.gridx = 2;
		gbc_ergebnissText.gridy = 3;
		Spiel.add(ergebnissText, gbc_ergebnissText);
		
		Button startButton = new Button("Start");
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				Spiel spiel = new Spiel(mannschaft1, mannschaft2);
				Gameplay gameplay = new Gameplay();
				try {
					gameplay.spielen(spiel);
					spielVerlaufText.setText(spiel.getSpielbericht().toString());
					ergebnissText.setText(spiel.getErgebnis().toString());
					Spiel.updateUI();
				} catch (SpielAbbruchException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		GridBagConstraints gbc_startButton = new GridBagConstraints();
		gbc_startButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_startButton.insets = new Insets(0, 0, 0, 5);
		gbc_startButton.gridx = 0;
		gbc_startButton.gridy = 3;
		Spiel.add(startButton, gbc_startButton);
		
		JLabel ergebnissLabel = new JLabel("Ergebniss:");
		GridBagConstraints gbc_ergebnissLabel = new GridBagConstraints();
		gbc_ergebnissLabel.insets = new Insets(0, 0, 0, 5);
		gbc_ergebnissLabel.gridx = 1;
		gbc_ergebnissLabel.gridy = 3;
		Spiel.add(ergebnissLabel, gbc_ergebnissLabel);
		
		JLabel dbLabel = new JLabel("Datenbank:");
		GridBagConstraints gbc_dbLabel = new GridBagConstraints();
		gbc_dbLabel.anchor = GridBagConstraints.EAST;
		gbc_dbLabel.insets = new Insets(0, 0, 5, 5);
		gbc_dbLabel.gridx = 0;
		gbc_dbLabel.gridy = 0;
		Datenbank.add(dbLabel, gbc_dbLabel);
		
		JLabel rolleLabel = new JLabel("Rolle");
		GridBagConstraints gbc_rolleLabel = new GridBagConstraints();
		gbc_rolleLabel.anchor = GridBagConstraints.EAST;
		gbc_rolleLabel.insets = new Insets(0, 0, 5, 5);
		gbc_rolleLabel.gridx = 0;
		gbc_rolleLabel.gridy = 1;
		Datenbank.add(rolleLabel, gbc_rolleLabel);
		
		datenbankDropdown = new JComboBox<String>();
		datenbankDropdown.setBackground(Color.LIGHT_GRAY);
		datenbankDropdown.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				String tmp = datenbankDropdown.getSelectedItem().toString();
				dataLayer = tmp;
				
				/*if(dataLayer.compareTo("Sqlite") == 0)
				{
					mannschaftDropdownDB.removeAllItems();
					for(int i = 1; i <= mannschaftList.getLength(); i++)
					{
						try {
							mannschaftDropdownDB.addItem(dataLayerSqlite.getMannschaftDao().read(i).getName());
						} catch (DaoException e1) {
							e1.printStackTrace();
						}
					}
				}
				
				if(dataLayer.compareTo("Xml") == 0)
				{
					mannschaftDropdownDB.removeAllItems();
					for(int i = 1; i <= mannschaftList.getLength(); i++)
					{
						try {
							mannschaftDropdownDB.addItem(dataLayerXml.getMannschaftDao().read(i).getName());
						} catch (DaoException e1) {
							e1.printStackTrace();
						}
					}
				}*/
				
				mannschaftDropdownDB.updateUI();
				
				contentPane.updateUI();
				Spiel.updateUI();
				Datenbank.updateUI();
			}
		});
		datenbankDropdown.setModel(new DefaultComboBoxModel(new String[] {"Xml", "Sqlite"}));
		GridBagConstraints gbc_datenbankDropdown = new GridBagConstraints();
		gbc_datenbankDropdown.insets = new Insets(0, 0, 5, 5);
		gbc_datenbankDropdown.fill = GridBagConstraints.HORIZONTAL;
		gbc_datenbankDropdown.gridx = 1;
		gbc_datenbankDropdown.gridy = 0;
		Datenbank.add(datenbankDropdown, gbc_datenbankDropdown);
		
		rolleDropdownDB = new JComboBox<String>();
		rolleDropdownDB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{	
				mannschaftDropdownDB.setVisible(false);
				motivationText.setVisible(false);
				staerkeText.setVisible(false);
				torschussText.setVisible(false);
				toreText.setVisible(false);
				reaktionText.setVisible(false);
				erfahrungText.setVisible(false);
				
				selectedRolle = rolleDropdownDB.getSelectedItem().toString();
				if(selectedRolle == "Spieler")
				{
					mannschaftDropdownDB.setVisible(true);
					motivationText.setVisible(true);
					staerkeText.setVisible(true);
					torschussText.setVisible(true);
					toreText.setVisible(true);
				}
				if(selectedRolle == "Trainer")
				{
					erfahrungText.setVisible(true);
				}
				if(selectedRolle == "Torwart")
				{
					reaktionText.setVisible(true);	
				}
				Datenbank.updateUI();
			}
		});
		rolleDropdownDB.setModel(new DefaultComboBoxModel(new String[] {"", "Spieler", "Torwart", "Trainer"}));
		GridBagConstraints gbc_rolleDropdownDB = new GridBagConstraints();
		gbc_rolleDropdownDB.insets = new Insets(0, 0, 5, 5);
		gbc_rolleDropdownDB.fill = GridBagConstraints.HORIZONTAL;
		gbc_rolleDropdownDB.gridx = 1;
		gbc_rolleDropdownDB.gridy = 1;
		Datenbank.add(rolleDropdownDB, gbc_rolleDropdownDB);
		
		JLabel nameLabel = new JLabel("Name:");
		GridBagConstraints gbc_nameLabel = new GridBagConstraints();
		gbc_nameLabel.insets = new Insets(0, 0, 5, 5);
		gbc_nameLabel.anchor = GridBagConstraints.EAST;
		gbc_nameLabel.gridx = 0;
		gbc_nameLabel.gridy = 2;
		Datenbank.add(nameLabel, gbc_nameLabel);
		
		nameText = new JTextField();
		nameText.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(nameText.getText() != null) name = nameText.getText();
			}
		});
		GridBagConstraints gbc_nameText = new GridBagConstraints();
		gbc_nameText.insets = new Insets(0, 0, 5, 5);
		gbc_nameText.fill = GridBagConstraints.HORIZONTAL;
		gbc_nameText.gridx = 1;
		gbc_nameText.gridy = 2;
		Datenbank.add(nameText, gbc_nameText);
		nameText.setColumns(10);
		
		JLabel alterLabel = new JLabel("Alter:");
		GridBagConstraints gbc_alterLabel = new GridBagConstraints();
		gbc_alterLabel.anchor = GridBagConstraints.EAST;
		gbc_alterLabel.insets = new Insets(0, 0, 5, 5);
		gbc_alterLabel.gridx = 0;
		gbc_alterLabel.gridy = 3;
		Datenbank.add(alterLabel, gbc_alterLabel);
		
		alterText = new JTextField();
		alterText.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(alterText.getText() != null) alter = Integer.parseInt(alterText.getText());
			}
		});
		GridBagConstraints gbc_alterText = new GridBagConstraints();
		gbc_alterText.insets = new Insets(0, 0, 5, 5);
		gbc_alterText.fill = GridBagConstraints.HORIZONTAL;
		gbc_alterText.gridx = 1;
		gbc_alterText.gridy = 3;
		Datenbank.add(alterText, gbc_alterText);
		alterText.setColumns(10);
		
		JLabel mannschaftDBLabel = new JLabel("Mannschaft:");
		GridBagConstraints gbc_mannschaftDBLabel = new GridBagConstraints();
		gbc_mannschaftDBLabel.anchor = GridBagConstraints.EAST;
		gbc_mannschaftDBLabel.insets = new Insets(0, 0, 5, 5);
		gbc_mannschaftDBLabel.gridx = 0;
		gbc_mannschaftDBLabel.gridy = 4;
		Datenbank.add(mannschaftDBLabel, gbc_mannschaftDBLabel);
		
		motivationText = new JTextField();
		motivationText.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(motivationText.getText() != null) motivation = Integer.parseInt(motivationText.getText());
			}
		});
		
		JLabel motivationLabel = new JLabel("Motivation");
		GridBagConstraints gbc_motivationLabel = new GridBagConstraints();
		gbc_motivationLabel.anchor = GridBagConstraints.EAST;
		gbc_motivationLabel.insets = new Insets(0, 0, 5, 5);
		gbc_motivationLabel.gridx = 0;
		gbc_motivationLabel.gridy = 5;
		Datenbank.add(motivationLabel, gbc_motivationLabel);
		motivationText.setToolTipText("Es sind nur Werte zwischen 1 - 10 erlaubt.");
		GridBagConstraints gbc_motivationText = new GridBagConstraints();
		gbc_motivationText.insets = new Insets(0, 0, 5, 5);
		gbc_motivationText.fill = GridBagConstraints.HORIZONTAL;
		gbc_motivationText.gridx = 1;
		gbc_motivationText.gridy = 5;
		Datenbank.add(motivationText, gbc_motivationText);
		motivationText.setColumns(10);
		
		JLabel staerkeLabel = new JLabel("St\u00E4rke:");
		GridBagConstraints gbc_staerkeLabel = new GridBagConstraints();
		gbc_staerkeLabel.anchor = GridBagConstraints.EAST;
		gbc_staerkeLabel.insets = new Insets(0, 0, 5, 5);
		gbc_staerkeLabel.gridx = 0;
		gbc_staerkeLabel.gridy = 6;
		Datenbank.add(staerkeLabel, gbc_staerkeLabel);
		
		staerkeText = new JTextField();
		staerkeText.setToolTipText("Es sind nur Werte zwischen 1 - 10 erlaubt.");
		staerkeText.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(staerkeText.getText() != null) staerke = Integer.parseInt(staerkeText.getText());
			}
		});
		GridBagConstraints gbc_staerkeText = new GridBagConstraints();
		gbc_staerkeText.insets = new Insets(0, 0, 5, 5);
		gbc_staerkeText.fill = GridBagConstraints.HORIZONTAL;
		gbc_staerkeText.gridx = 1;
		gbc_staerkeText.gridy = 6;
		Datenbank.add(staerkeText, gbc_staerkeText);
		staerkeText.setColumns(10);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_horizontalStrut.gridx = 3;
		gbc_horizontalStrut.gridy = 7;
		Datenbank.add(horizontalStrut, gbc_horizontalStrut);
		
		JTextArea erklaerungText = new JTextArea();
		erklaerungText.setFont(new Font("Rockwell", Font.PLAIN, 14));
		erklaerungText.setText("Person einf\u00FCgen / updaten:\r\n- alle Daten eingeben und anschlie\u00DFend\r\n  auf den Button \"Einf\u00FCgen\" klicken.\r\n\r\nSpieler / Mannschaft suchen:\r\n- den Namen / Mannschaft ausf\u00FCllen und anschlie\u00DFend\r\n  auf den Button \"Suchen\" klicken. \r\n\r\nPerson l\u00F6schen:\r\n- den Namen und Rolle ausf\u00FCllen und anschlie\u00DFend\r\n  auf den Button \"L\u00F6schen\" klicken.\r\n");
		erklaerungText.setColumns(6);
		erklaerungText.setEditable(false);
		GridBagConstraints gbc_erklaerungText = new GridBagConstraints();
		gbc_erklaerungText.gridheight = 13;
		gbc_erklaerungText.insets = new Insets(0, 0, 5, 0);
		gbc_erklaerungText.gridx = 4;
		gbc_erklaerungText.gridy = 1;
		Datenbank.add(erklaerungText, gbc_erklaerungText);
		
		JLabel torschussLabel = new JLabel("Torschuss:");
		GridBagConstraints gbc_torschussLabel = new GridBagConstraints();
		gbc_torschussLabel.anchor = GridBagConstraints.EAST;
		gbc_torschussLabel.insets = new Insets(0, 0, 5, 5);
		gbc_torschussLabel.gridx = 0;
		gbc_torschussLabel.gridy = 8;
		Datenbank.add(torschussLabel, gbc_torschussLabel);
		
		torschussText = new JTextField();
		torschussText.setToolTipText("Es sind nur Werte zwischen 1 - 10 erlaubt.");
		torschussText.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(torschussText.getText() != null) torschuss = Integer.parseInt(torschussText.getText());
			}
		});
		GridBagConstraints gbc_torschussText = new GridBagConstraints();
		gbc_torschussText.insets = new Insets(0, 0, 5, 5);
		gbc_torschussText.fill = GridBagConstraints.HORIZONTAL;
		gbc_torschussText.gridx = 1;
		gbc_torschussText.gridy = 8;
		Datenbank.add(torschussText, gbc_torschussText);
		torschussText.setColumns(10);
		
		JLabel toreLabel = new JLabel("Tore:");
		GridBagConstraints gbc_toreLabel = new GridBagConstraints();
		gbc_toreLabel.anchor = GridBagConstraints.EAST;
		gbc_toreLabel.insets = new Insets(0, 0, 5, 5);
		gbc_toreLabel.gridx = 0;
		gbc_toreLabel.gridy = 9;
		Datenbank.add(toreLabel, gbc_toreLabel);
		
		toreText = new JTextField();
		toreText.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if(toreText.getText() != null) tore = Integer.parseInt(toreText.getText());
			}
		});
		GridBagConstraints gbc_toreText = new GridBagConstraints();
		gbc_toreText.insets = new Insets(0, 0, 5, 5);
		gbc_toreText.fill = GridBagConstraints.HORIZONTAL;
		gbc_toreText.gridx = 1;
		gbc_toreText.gridy = 9;
		Datenbank.add(toreText, gbc_toreText);
		toreText.setColumns(10);
		
		JLabel reaktionLabel = new JLabel("Reaktion:");
		reaktionLabel.setToolTipText("Dieser Wert wird nur f\u00FCr den Torwart ben\u00F6tigt.");
		GridBagConstraints gbc_reaktionLabel = new GridBagConstraints();
		gbc_reaktionLabel.anchor = GridBagConstraints.EAST;
		gbc_reaktionLabel.insets = new Insets(0, 0, 5, 5);
		gbc_reaktionLabel.gridx = 0;
		gbc_reaktionLabel.gridy = 10;
		Datenbank.add(reaktionLabel, gbc_reaktionLabel);
		
		reaktionText = new JTextField();
		reaktionText.setToolTipText("Es sind nur Werte zwischen 1 - 10 erlaubt.");
		GridBagConstraints gbc_reaktionText = new GridBagConstraints();
		gbc_reaktionText.insets = new Insets(0, 0, 5, 5);
		gbc_reaktionText.fill = GridBagConstraints.HORIZONTAL;
		gbc_reaktionText.gridx = 1;
		gbc_reaktionText.gridy = 10;
		Datenbank.add(reaktionText, gbc_reaktionText);
		reaktionText.setColumns(10);
		
		JLabel erfahrungLabel = new JLabel("Erfahrung:");
		erfahrungLabel.setToolTipText("Dieser Wert wird nur f\u00FCr den Trainer ben\u00F6tigt.");
		GridBagConstraints gbc_erfahrungLabel = new GridBagConstraints();
		gbc_erfahrungLabel.anchor = GridBagConstraints.EAST;
		gbc_erfahrungLabel.insets = new Insets(0, 0, 5, 5);
		gbc_erfahrungLabel.gridx = 0;
		gbc_erfahrungLabel.gridy = 11;
		Datenbank.add(erfahrungLabel, gbc_erfahrungLabel);
		
		erfahrungText = new JTextField();
		erfahrungText.setToolTipText("Es sind nur Werte zwischen 1 - 10 erlaubt.");
		GridBagConstraints gbc_erfahrungText = new GridBagConstraints();
		gbc_erfahrungText.fill = GridBagConstraints.HORIZONTAL;
		gbc_erfahrungText.insets = new Insets(0, 0, 5, 5);
		gbc_erfahrungText.gridx = 1;
		gbc_erfahrungText.gridy = 11;
		Datenbank.add(erfahrungText, gbc_erfahrungText);
		erfahrungText.setColumns(10);
		
		JButton suchenButton = new JButton("Suchen");
		suchenButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				createGUI instance = null;
				try {
					instance = new createGUI();
				} catch (DaoException e1) {
					e1.printStackTrace();
				}
				if(nameText.getText().isEmpty() && !selectedMannschaftDropdownDB.isEmpty())
				{
					int i = instance.getMannschaftID(selectedMannschaftDropdownDB.toString());
					String tmp;
					try {
						tmp = dataLayerXml.getMannschaftDao().read(i).toString();
						verlaufText.setText(tmp);
					} catch (DaoException e1) {
						e1.printStackTrace();
					}
				}
				else
				{
					if(selectedRolle.compareTo("Spieler") == 0)
					{
						int i = instance.getSpielerID(nameText.getText());
						String tmp = "";
						try {
							if(dataLayer.compareTo("Xml") == 0) tmp = dataLayerXml.getSpielerDao().read(i).toString();
							else  tmp = dataLayerSqlite.getSpielerDao().read(i).toString();
							verlaufText.setText(tmp);
						} catch (DaoException e1) {
							e1.printStackTrace();
						}
					}
					if(selectedRolle.compareTo("Torwart") == 0)
					{
						int i = instance.getTorwartID(nameText.getText());
						String tmp = "";
						try {
							if(dataLayer.compareTo("Xml") == 0) tmp = dataLayerXml.getTorwartDao().read(i).toString();
							else tmp = dataLayerSqlite.getTorwartDao().read(i).toString();
							verlaufText.setText(tmp);
						} catch (DaoException e1) {
							e1.printStackTrace();
						}
					}
					if(selectedRolle.compareTo("Trainer") == 0)
					{
						int i = instance.getTrainerID(nameText.getText());
						String tmp = "";
						try {
							if(dataLayer.compareTo("Xml") == 0) tmp = dataLayerXml.getTrainerDao().read(i).toString();
							else tmp = dataLayerSqlite.getTrainerDao().read(i).toString();
							verlaufText.setText(tmp);
						} catch (DaoException e1) {
							e1.printStackTrace();
						}
					}
				}
			}
		});
		
		JButton einfuegenButton = new JButton("Einf\u00FCgen");
		einfuegenButton.setForeground(Color.BLACK);
		einfuegenButton.setBackground(Color.LIGHT_GRAY);
		einfuegenButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) 
			{
				createGUI instance = null;
				try {
					instance = new createGUI();
				} catch (DaoException e1) {
					e1.printStackTrace();
				}
				
				if(instance.getSpielerID(name) == -1)
				{		
					instance.createSpieler(name, alter, motivation, staerke, torschuss, tore, selectedMannschaftDropdownDB);
					if(instance.createSpieler(name, alter, motivation, staerke, torschuss, tore, selectedMannschaftDropdownDB)) verlaufText.setText("Der Spieler wurde hinzugef�gt.");
					else verlaufText.setText("Bitte f�llen Sie alle Felder aus! \n"
							+ "Bitte achten Sie darauf das nur die Zul�ssigen Werte in die Felder eingetragen sind!");
				}
				else
				{
					instance.updateSpieler(name, alter, motivation, staerke, torschuss, tore, selectedMannschaftDropdownDB);
					if(instance.updateSpieler(name, alter, motivation, staerke, torschuss, tore, selectedMannschaftDropdownDB)) verlaufText.setText("Die Spieler Daten wurden aktualisiert.");
					else verlaufText.setText("Bitte achten Sie darauf das nur die Zul�ssigen Werte in die Felder eingetragen sind!");
				}
				
				if(instance.getTrainerID(name) == -1)
				{		
					instance.createTrainer(name, alter, erfahrung);
					if(instance.createTrainer(name, alter, erfahrung)) verlaufText.setText("Der Trainer wurde hinzugef�gt.");
					else verlaufText.setText("Bitte f�llen Sie alle Felder aus! \n"
							+ "Bitte achten Sie darauf das nur die Zul�ssigen Werte in die Felder eingetragen sind!");
				}
				else
				{
					instance.updateTrainer(name, alter, erfahrung);
					if(instance.updateTrainer(name, alter, erfahrung)) verlaufText.setText("Die Trainer Daten wurden aktualisiert.");
					else verlaufText.setText("Bitte achten Sie darauf das nur die Zul�ssigen Werte in die Felder eingetragen sind!");
				}
				
				if(instance.getTorwartID(name) == -1)
				{		
					instance.createTorwart(name, alter, staerke, motivation, reaktion);
					if(instance.createTorwart(name, alter, staerke, motivation, reaktion)) verlaufText.setText("Der Torwart wurde hinzugef�gt.");
					else verlaufText.setText("Bitte f�llen Sie alle Felder aus! \n"
							+ "Bitte achten Sie darauf das nur die Zul�ssigen Werte in die Felder eingetragen sind!");
				}
				else
				{
					instance.updateTorwart(name, alter, staerke, motivation, reaktion);
					if(instance.updateTorwart(name, alter, staerke, motivation, reaktion)) verlaufText.setText("Die Torwart Daten wurden aktualisiert.");
					else verlaufText.setText("Bitte achten Sie darauf das nur die Zul�ssigen Werte in die Felder eingetragen sind!");
				}
			}
		});
		
		Component verticalStrut = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut = new GridBagConstraints();
		gbc_verticalStrut.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut.gridx = 1;
		gbc_verticalStrut.gridy = 12;
		Datenbank.add(verticalStrut, gbc_verticalStrut);
		GridBagConstraints gbc_einfuegenButton = new GridBagConstraints();
		gbc_einfuegenButton.insets = new Insets(0, 0, 5, 5);
		gbc_einfuegenButton.gridx = 0;
		gbc_einfuegenButton.gridy = 13;
		Datenbank.add(einfuegenButton, gbc_einfuegenButton);
		GridBagConstraints gbc_suchenButton = new GridBagConstraints();
		gbc_suchenButton.insets = new Insets(0, 0, 5, 5);
		gbc_suchenButton.gridx = 1;
		gbc_suchenButton.gridy = 13;
		Datenbank.add(suchenButton, gbc_suchenButton);
		
		JButton loeschenButton = new JButton("L\u00F6schen");
		loeschenButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
				createGUI instance = null;
				try {
					instance = new createGUI();
				} catch (DaoException e1) {
					e1.printStackTrace();
				}
				if(selectedRolle.compareTo("Spieler") == 0)
				{
					instance.deletSpieler(instance.getSpielerID(name));
					verlaufText.setText("Der Eintrag des Spielers wurde gel�scht.");
				}
				
				if(selectedRolle.compareTo("Trainer") == 0)
				{
					instance.deletTrainer(instance.getTrainerID(name));
					verlaufText.setText("Der Eintrag des Trainers wurde gel�scht.");
				}
				
				if(selectedRolle.compareTo("Torwart") == 0)
				{
					instance.deletTorwart(instance.getTorwartID(name));
					verlaufText.setText("Der Eintrag des Torwarts wurde gel�scht.");
				}
			}
		});
		GridBagConstraints gbc_loeschenButton = new GridBagConstraints();
		gbc_loeschenButton.insets = new Insets(0, 0, 5, 5);
		gbc_loeschenButton.gridx = 2;
		gbc_loeschenButton.gridy = 13;
		Datenbank.add(loeschenButton, gbc_loeschenButton);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridwidth = 4;
		gbc_scrollPane.gridx = 1;
		gbc_scrollPane.gridy = 16;
		Datenbank.add(scrollPane, gbc_scrollPane);
		
		verlaufText = new JTextArea();
		scrollPane.setViewportView(verlaufText);
		verlaufText.setEditable(false);
		
		Component verticalStrut_3 = Box.createVerticalStrut(20);
		GridBagConstraints gbc_verticalStrut_3 = new GridBagConstraints();
		gbc_verticalStrut_3.insets = new Insets(0, 0, 5, 5);
		gbc_verticalStrut_3.gridx = 1;
		gbc_verticalStrut_3.gridy = 15;
		Datenbank.add(verticalStrut_3, gbc_verticalStrut_3);
		
		JLabel logLabel = new JLabel("Verlauf:");
		GridBagConstraints gbc_logLabel = new GridBagConstraints();
		gbc_logLabel.anchor = GridBagConstraints.NORTH;
		gbc_logLabel.insets = new Insets(0, 0, 0, 5);
		gbc_logLabel.gridx = 0;
		gbc_logLabel.gridy = 16;
		Datenbank.add(logLabel, gbc_logLabel);
	}
	
	public Boolean createSpieler(String name, int alter, int motivation, int staerke, int torschuss, int tore, String selectedMannschaftDropdownDB)
	{
		createGUI instance = null;
		try {
			instance = new createGUI();
		} catch (DaoException e1) {
			e1.printStackTrace();
		}
		
		ISpieler spieler = null;
		if(!name.isEmpty() && alter > 0 && motivation >= 0 && motivation <= 10 &&
				staerke >= 0 && staerke <= 10 && tore >= 0 && torschuss >= 0 && torschuss <= 10 &&
				selectedMannschaftDropdownDB != "") spieler = new Spieler(name, alter, staerke, torschuss, motivation, tore);
		if(spieler != null)
		{
			try {
				if(dataLayer.compareTo("Xml") == 0) dataLayerXml.getSpielerDao().create(spieler, instance.getMannschaftID(selectedMannschaftDropdownDB));
				else dataLayerSqlite.getSpielerDao().create(spieler, instance.getMannschaftID(selectedMannschaftDropdownDB));
			} catch (DaoException e1) {
				e1.printStackTrace();
			}
			return true;
		}
		else return false;
	}
	
	public Boolean createTorwart(String name, int alter, int staerke, int motivation, int reaktion)
	{
		createGUI instance = null;
		try {
			instance = new createGUI();
		} catch (DaoException e1) {
			e1.printStackTrace();
		}
		
		ITorwart torwart = null;
		if(!name.isEmpty() && alter > 0 && motivation >= 0 && motivation <= 10 && staerke >= 0 && staerke <= 10) torwart = new Torwart(name, alter, staerke, motivation, reaktion);
		if(torwart != null)
		{
			try {
				if(dataLayer.compareTo("Xml") == 0) dataLayerXml.getTorwartDao().create(torwart);
				else dataLayerSqlite.getTorwartDao().create(torwart);
			} catch (DaoException e1) {
				e1.printStackTrace();
			}
			return true;
		}
		else return false;
	}
	
	public Boolean createTrainer(String name, int alter, int erfahrung)
	{
		createGUI instance = null;
		try {
			instance = new createGUI();
		} catch (DaoException e1) {
			e1.printStackTrace();
		}
		
		ITrainer trainer = null;
		if(!name.isEmpty() && alter > 0 && erfahrung >= 0 && erfahrung <= 10) trainer = new Trainer(name, alter, erfahrung);
		if(trainer != null)
		{
			try {
				if(dataLayer.compareTo("Xml") == 0) dataLayerXml.getTrainerDao().create(trainer);
				else dataLayerSqlite.getTrainerDao().create(trainer);
			} catch (DaoException e1) {
				e1.printStackTrace();
			}
			return true;
		}
		else return false;
	}
	
	public Boolean updateSpieler(String name, int alter, int motivation, int staerke, int torschuss, int tore, String selectedMannschaftDropdownDB)
	{
		createGUI instance = null;
		try {
			instance = new createGUI();
		} catch (DaoException e1) {
			e1.printStackTrace();
		}
		
		if(!name.isEmpty() || alter <= 0 || motivation < 0 || motivation > 10 ||
				staerke < 0 || staerke > 10 || tore < 0 || torschuss < 0 | torschuss > 10 ||
				selectedMannschaftDropdownDB != "")
		{
			ISpieler spieler = null;
			spieler = new Spieler(name, alter, staerke, torschuss, motivation, tore);
			try {
				if(dataLayer.compareTo("Xml") == 0)
				{
					dataLayerXml.getSpielerDao().update(spieler);
					dataLayerXml.getSpielerDao().update(spieler, instance.getMannschaftID(selectedMannschaftDropdownDB));
				}
				else
				{
					dataLayerSqlite.getSpielerDao().update(spieler);
					dataLayerSqlite.getSpielerDao().update(spieler, instance.getMannschaftID(selectedMannschaftDropdownDB));
				}
				
				return true;
			} catch (DaoException e1) {
				e1.printStackTrace();
			}
		}
		return false;
	}
	
	public Boolean updateTrainer(String name, int alter, int erfahrung)
	{
		createGUI instance = null;
		try {
			instance = new createGUI();
		} catch (DaoException e1) {
			e1.printStackTrace();
		}
		
		if(!name.isEmpty() || alter <= 0 || erfahrung < 0 || erfahrung > 10)
		{
			ITrainer trainer = null;
			trainer = new Trainer(name, alter, erfahrung);
			try {
				if(dataLayer.compareTo("Xml") == 0) dataLayerXml.getTrainerDao().update(trainer);
				else dataLayerSqlite.getTrainerDao().update(trainer);
				return true;
			} catch (DaoException e1) {
				e1.printStackTrace();
			}
		}
		return false;
	}
	
	public Boolean updateTorwart(String name, int alter, int staerke, int motivation, int reaktion)
	{
		createGUI instance = null;
		try {
			instance = new createGUI();
		} catch (DaoException e1) {
			e1.printStackTrace();
		}
		
		if(!name.isEmpty() || alter <= 0 || reaktion < 0 || reaktion > 10)
		{
			ITorwart torwart = null;
			torwart = new Torwart(name, alter, motivation, staerke, reaktion);
			try {
				if(dataLayer.compareTo("Xml") == 0) dataLayerXml.getTorwartDao().update(torwart);
				else dataLayerSqlite.getTorwartDao().update(torwart);
				return true;
			} catch (DaoException e1) {
				e1.printStackTrace();
			}
		}
		return false;
	}
	
	public void deletSpieler(int id)
	{
		try {
			if(dataLayer.compareTo("Xml") == 0) dataLayerXml.getSpielerDao().delete(dataLayerXml.getSpielerDao().read(id));
			else dataLayerSqlite.getSpielerDao().delete(dataLayerSqlite.getSpielerDao().read(id));
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}
	
	public void deletMannschaft(int id)
	{
		try {
			if(dataLayer.compareTo("Xml") == 0) dataLayerXml.getMannschaftDao().delete(dataLayerXml.getMannschaftDao().read(id));
			else dataLayerSqlite.getMannschaftDao().delete(dataLayerSqlite.getMannschaftDao().read(id));
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}
	
	public void deletTrainer(int id)
	{
		try {
			if(dataLayer.compareTo("Xml") == 0) dataLayerXml.getTrainerDao().delete(dataLayerXml.getTrainerDao().read(id));
			else dataLayerSqlite.getTrainerDao().delete(dataLayerSqlite.getTrainerDao().read(id));
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}
	
	public void deletTorwart(int id)
	{
		try {
			if(dataLayer.compareTo("Xml") == 0) dataLayerXml.getTorwartDao().delete(dataLayerXml.getTorwartDao().read(id));
			else dataLayerSqlite.getTorwartDao().delete(dataLayerSqlite.getTorwartDao().read(id));
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}
	
	public int getMannschaftID(String mannschaft)
	{
		for(int i = 1; i <= mannschaftList.getLength(); i++)
		{
			try {
				if(dataLayer.compareTo("Xml") == 0) if(mannschaft.compareTo(dataLayerXml.getMannschaftDao().read(i).getName()) == 0) return i;
				else if(mannschaft.compareTo(dataLayerSqlite.getMannschaftDao().read(i).getName()) == 0) return i;
			} catch (DaoException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}
	
	public int getSpielerID(String spieler)
	{
		for(int i = 1; i <= spielerList.getLength(); i++)
		{		
			try {
				if(dataLayer.compareTo("Xml") == 0) if(spieler.compareTo(dataLayerXml.getSpielerDao().read(i).getName()) == 0) return i;
				else if(spieler.compareTo(dataLayerSqlite.getSpielerDao().read(i).getName()) == 0) return i;
			} catch (DaoException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}
	
	public int getTrainerID(String trainer)
	{
		for(int i = 1; i <= trainerList.getLength(); i++)
		{
			try {
				if(dataLayer.compareTo("Xml") == 0) if(trainer.compareTo(dataLayerXml.getTrainerDao().read(i).getName()) == 0) return i;
				else if(trainer.compareTo(dataLayerSqlite.getTrainerDao().read(i).getName()) == 0) return i;
			} catch (DaoException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}
	
	public int getTorwartID(String torwart)
	{
		for(int i = 1; i <= torwartList.getLength(); i++)
		{
			try {
				if(dataLayer.compareTo("Xml") == 0) if(torwart.compareTo(dataLayerXml.getTorwartDao().read(i).getName()) == 0) return i;
				else if(torwart.compareTo(dataLayerSqlite.getTorwartDao().read(i).getName()) == 0) return i;
			} catch (DaoException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}

}

